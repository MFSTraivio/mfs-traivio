package com.cobrapm.websocket;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.log4j.Logger;

import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.google.gson.Gson;


@ServerEndpoint("/messages")
public class Messager {
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private final Logger log = Logger.getLogger(getClass().getName());
	private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
//	private static Set<UserMessageRecord> users = Collections.synchronizedSet(new HashSet<UserMessageRecord>());
	private static String userid = "";
	private static String lib = "";
 
	@OnMessage
	public void onMessage(Session session, String message) {
		MessageRecord messenger = new MessageRecord();
		boolean check = false;
		String messageOut = "";
		if (message != null) {
			String parts[] =  message.split(",");
			if(parts[0].equals("UserID")){
				check = true;
				userid = parts[1];
			}
			if(parts[0].equals("Lib")){
				check = true;
				lib = parts[1];
			}
			if(userid != "" && lib != "" &&
					userid != "undefined" && lib != "undefined"){
				UserMessageRecord tmp = new UserMessageRecord();
				tmp.UserID = userid;
				tmp.Lib = lib;
				log.info("Added user: " + userid + ", lib: " + lib);
				synchronized(peers){
					for(Session s : peers){
						if(s.equals(session)){
							s.getUserProperties().put("userInf", tmp);
						}
					}
				}
			}

			if(!check){
				CONNECTOR CONNECTOR = new CONNECTOR(); 
		 		Connection conn = null;
		 		String key1 = "";
		 		String key2 = "";
		 		synchronized(peers){
		 		for(Session s : peers) {
		 			try{	    
		 				UserMessageRecord userInf = new UserMessageRecord();
		 				userInf = (UserMessageRecord) s.getUserProperties().get("userInf");
		 				
		 				CONNECTOR = connectionHelper.getConnection(userInf.Lib);
		 				conn = CONNECTOR.conn;
		 		        log.info(userInf.UserID);
		 				CallableStatement stmt = null;
		 				String query = "CALL SPMSGQCR00(?,?,?)";
		 				try {
		 					stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		 					stmt.setString(1, userInf.UserID);
		 					stmt.setString(2, message);
		 					stmt.setString(3, "Y");
		
		 					stmt.execute();
		 			    	
		 					ResultSet rs = stmt.getResultSet();
		 			    
		 					while(rs.next()){
		 						switch(message){
		 						case "PNOTE2" :
		 							messenger.MsgTitle = "New Note";
		 						}
		 						messenger.MsgText = rs.getString(2).trim();
		 						try{
		 							key1 = rs.getString(4).trim();
		 							key2 = rs.getString(5).trim();
		 						}catch(Exception e){
		 					  	
		 						}
		 						messenger.MsgRead = rs.getString(6).trim();
		 					}
		 			   
		 					stmt.close();
		 			        
		 				} catch (SQLException e ) {
		 			    	log.info(e);
		 				} finally {
		 					if (stmt != null) { stmt.close(); }
		 				}
		 				stmt = null;
		 				log.info(key1);
		 				switch(message){
		 					case "PNOTE2" :
		 						if(key1 != ""){
		 						query = "CALL SPNRELCR01(?)";
		 						try {
		 							stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		 							stmt.setInt(1, Integer.parseInt(key1));		
		 							stmt.execute();
		 						
		 							ResultSet rs = stmt.getResultSet();
		 							int count = 0;
		 							while(rs.next()){
		 								if(count == 0){
		 									messenger.MsgText += " Related To " + rs.getString(1).trim();
		 									count ++;
		 								} else {
		 									messenger.MsgText += ", " + rs.getString(1).trim();
		 								}
		 							}
		 			   
		 							stmt.close();
		 			        
		 						} catch (Exception e ) {
		 							log.info(e);
		 						} finally {
		 							if (stmt != null) { stmt.close(); }
		 						}
		 						}
		 				}
		 			    messageOut = new Gson().toJson(messenger);
		 				try {
							s.getBasicRemote().sendText(messageOut);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		 				
		 				connectionHelper.closeConnection(conn);
		 			} catch(Exception ex) {
		 				messageOut = "An error occured during the authenticate process.";
		 			}
		 		}
		 		}
			}
		}
	}
	
	 @OnOpen
	 public void onOpen(Session peer) {
		 log.info("Connection opened ...");
		 synchronized(peers){
			 peers.add(peer);
			 log.info(peers.size());
		 }
	 }
	 @OnClose
	 public void onClose(Session peer) {
	     log.info("Connection closed ...");
	     synchronized(peers){
	    	 peers.remove(peer);
	     }
	 }
}


