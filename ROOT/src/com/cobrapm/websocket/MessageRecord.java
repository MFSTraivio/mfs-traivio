package com.cobrapm.websocket;

public class MessageRecord {
	public String MsgTitle;
	public String MsgText;
	public String MsgLink;
	public String MsgType;
	public String MsgKey1;
	public String MsgKey2;
	public String MsgRead;
	
	public MessageRecord(
					String MsgTitlein,
					String MsgTextin,
					String MsgLinkin,
					String MsgTypein,
					String MsgKey1in,
					String MsgKey2in,
					String MsgReadin){
		MsgTitle = MsgTitlein;
		MsgText = MsgTextin;
		MsgLink = MsgLinkin;
		MsgType = MsgTypein;
		MsgKey1 = MsgKey1in;
		MsgKey2 = MsgKey2in;
		MsgRead = MsgReadin;
	}

	public MessageRecord() {
		MsgTitle = "";
		MsgText = "";
		MsgLink = "";
		MsgType = "";
		MsgKey1 = "";
		MsgKey2 = "";
		MsgRead = "";
	}	
}
