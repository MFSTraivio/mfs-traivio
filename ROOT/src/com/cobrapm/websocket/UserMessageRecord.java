package com.cobrapm.websocket;

public class UserMessageRecord {
	public String UserID;
	public String Lib;
	
	public UserMessageRecord(
					String UserIDin,
					String Libin){
		UserID = UserIDin;
		Lib = Libin;
	}

	public UserMessageRecord() {
		UserID = "";
		Lib = "";
	}	
}
