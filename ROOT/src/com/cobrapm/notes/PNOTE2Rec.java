package com.cobrapm.notes;

import java.sql.Timestamp;
import java.util.ArrayList;


public class PNOTE2Rec {
	
	public int N5ID;
	public int N5IDCMTSEQ;
	public int N5KEYN1;
	public int N5KEYN2;
	public int N5KEYN3;
	public String N5TEXT;
	public String N5PINNED;
	public Timestamp SYSCRT;
	public String SYSCRTBY;
	public String ProcessDesc;
	public Timestamp SYSUPD;
	public String SYSUPDBY;
	public String TagDesc;
	public int count;
	public String editForm;
	public ArrayList<PNOTE2ATTRec> PNOTE2ATTRecs;
	public ArrayList<PNOTE2REL> PNOTE2RELs;
	
	public PNOTE2Rec(int N5IDin, 
				    int N5IDCMTSEQin,
				    int N5KEYN1in,
				    int N5KEYN2in,
				    int N5KEYN3in,
				    String N5TEXTin,
				    String N5PINNEDin,
				    Timestamp SYSCRTin,
				    String SYSCRTBYin,
				    String ProcessDescin,
				    Timestamp SYSUPDin,
				    String SYSUPDBYin,
				    String TagDescin,
				    int countin,
				    String editFormin,
				    ArrayList<PNOTE2ATTRec> PNOTE2ATTRecsin,
				    ArrayList<PNOTE2REL> PNOTE2RELsin){
		N5ID = N5IDin;
		N5IDCMTSEQ = N5IDCMTSEQin;
		N5KEYN1 = N5KEYN1in;
		N5KEYN2 = N5KEYN2in;
		N5KEYN3 = N5KEYN3in;
		N5TEXT = N5TEXTin;
		N5PINNED = N5PINNEDin;
		SYSCRT = SYSCRTin;
		SYSCRTBY = SYSCRTBYin;
		ProcessDesc = ProcessDescin;
		SYSUPD = SYSUPDin;
		SYSUPDBY = SYSUPDBYin;
		TagDesc = TagDescin;
		count = countin;
		editForm = editFormin;
		PNOTE2ATTRecs = PNOTE2ATTRecsin;
		PNOTE2RELs = PNOTE2RELsin;
	}

	public PNOTE2Rec() {
		N5ID = 0;
		N5IDCMTSEQ = 0;
		N5KEYN1 = 0;
		N5KEYN2 = 0;
		N5KEYN3 = 0;
		N5TEXT = "";
		N5PINNED = "";
		SYSCRT = null;
		SYSCRTBY = "";
		ProcessDesc = "";
		SYSUPD = null;
		SYSUPDBY = "";
		TagDesc = "";
		count = 0;
		editForm = "";
		PNOTE2ATTRecs = new ArrayList<PNOTE2ATTRec>();
		PNOTE2RELs = new ArrayList<PNOTE2REL>();
	}
}
