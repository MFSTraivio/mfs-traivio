package com.cobrapm.notes;

import java.util.ArrayList;

import com.cobrapm.doa.PRELDESC;


public class InsertNoteRecord {
	public String lib;
	public String userid;
	public String noteText;
	public String isPinned;
	public ArrayList<PRELDESC> descsOut;
	public int KEYN1;
	public int KEYN2;
	public int KEYN3;
	public String documentFolder;
	
	public InsertNoteRecord(	String libin,
								String useridin,
								String noteTextin,
								String isPinnedin,
								ArrayList<PRELDESC> descsOutin,
								int KEYN1in,
								int KEYN2in,
								int KEYN3in,
								String documentFolderin){
		
		lib = libin;
		userid = useridin;
		noteText = noteTextin;
		isPinned = isPinnedin;
		descsOut = descsOutin;
		KEYN1 = KEYN1in;
		KEYN2 = KEYN2in;
		KEYN3 = KEYN3in;
		documentFolder = documentFolderin;
	}

	public InsertNoteRecord() {
		lib = "";
		userid = "";
		noteText = "";
		isPinned = "";
		descsOut = null;
		KEYN1 = 0;
		KEYN2 = 0;
		KEYN3 = 0;
		documentFolder = "";
	}
}
