package com.cobrapm.notes;

import java.util.ArrayList;

import org.codehaus.jackson.type.TypeReference;


public class UpdateNoteRecord {
	public String lib;
	public String userid;
	public String noteText;
	public int noteID;
	public String mode;
	
	public UpdateNoteRecord(	String libin,
								String useridin,
								String noteTextin,
								int noteIDin,
								String modein){
		
		lib = libin;
		userid = useridin;
		noteText = noteTextin;
		noteID = noteIDin;
		mode = modein;
	}

	public UpdateNoteRecord() {
		lib = "";
		userid = "";
		noteText = "";
		noteID = 0;
		mode = "";
	}
}
