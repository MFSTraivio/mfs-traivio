package com.cobrapm.process;

public class UDFSubData {
	
	public int DPSEQ;
	public int DSSEQ;
	public String DSVAL;
	
	public UDFSubData(	int DPSEQin, 
				    int DSSEQin,
				    String DSVALin){
		DPSEQ = DPSEQin;
		DSSEQ = DSSEQin;
		DSVAL = DSVALin;
	}

	public UDFSubData() {
		DPSEQ = 0;
		DSSEQ = 0;
		DSVAL = "";
	}
}
