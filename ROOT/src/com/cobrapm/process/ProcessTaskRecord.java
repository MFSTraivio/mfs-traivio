package com.cobrapm.process;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"selected","StatusIcon","ActStartDateFmt","ActCompDateFmt","showPopover"})
public class ProcessTaskRecord {
	public int TSTKNO;
	public int TSTSKNO;
	public int TSTSKSNO;
	public String TSTYPE;
	public String Assigned;
	public String Group;
	public String Desc;
	public String Frequency;
	public String Rollover;
	public String Status;
	public String EstStartDate;
	public String ActStartDate;
	public String DaysCompInd;
	public int DaysComp;
	public String DueDate;
	public String ActCompDate;
	public int ExpDuration;
	public int ActDuration;
	public String ReasonCodeTable;
	public String ReasonCodeReq;
	public String ReasonCode;
	public String Severity;
	public String previousStatus;
	public int Order;
	public int Interval;
	public String UserTaskStatus;
	public String AutoStart;
	public String Depend;
	public String NextRecurReleased;
	public int TaskID;
	public String WeekDay;
	public int MonthDay;
	public int FreqDays;
	public String Closed;
	public BigDecimal EstHours;
	public String SYSCRT;
	public String SYSUPD;
	public String SYSCRTBY;
	public String SYSUPDBY;
	public String PGMCRTBY;
	public String PGMUPDBY;
	public String RecentNote;
	public String AssignedBy;
	public String AssignedByTime;
	public String StopRecurDate;
	public String OriginalUser;
	public String OriginalUserTime;
	public String CurUser;
	public String CurUserTime;
	public int RecurMonth;
	public String SupProcType;
	public int SubTKNO;
	public String Cat;
	public String SubCat;
	public String SubCat2;
	public String SubCat3;
	public String previousStat;
	public String Mode;
	
	public ProcessTaskRecord(	int TSTKNOin,
								int TSTSKNOin,
								int TSTSKSNOin,
								String TSTYPEin,
								String Assignedin,
								String Groupin,
								String Descin,
								String Frequencyin,
								String Rolloverin,
								String Statusin,
								String EstStartDatein,
								String ActStartDatein,
								String DaysCompIndin,
								int DaysCompin,
								String DueDatein,
								String ActCompDatein,
								int ExpDurationin,
								int ActDurationin,
								String ReasonCodeTablein,
								String ReasonCodeReqin,
								String ReasonCodein,
								String Severityin,
								String previousStatusin,
								int Orderin,
								int Intervalin,
								String UserTaskStatusin,
								String AutoStartin,
								String Dependin,
								String NextRecurReleasedin,
								int TaskIDin,
								String WeekDayin,
								int MonthDayin,
								int FreqDaysin,
								String Closedin,
								BigDecimal EstHoursin,
								String SYSCRTin,
								String SYSUPDin,
								String SYSCRTBYin,
								String SYSUPDBYin,
								String PGMCRTBYin,
								String PGMUPDBYin,
								String RecentNotein,
								String AssignedByin,
								String AssignedByTimein,
								String StopRecurDatein,
								String OriginalUserin,
								String OriginalUserTimein,
								String CurUserin,
								String CurUserTimein,
								int RecurMonthin,
								String SupProcTypein,
								int SubTKNOin,
								String Catin,
								String SubCatin,
								String SubCat2in,
								String SubCat3in,
								String previousStatin,
								String Modein){
		
		TSTKNO = TSTKNOin;
		TSTSKNO = TSTSKNOin;
		TSTSKSNO = TSTSKSNOin;
		TSTYPE = TSTYPEin;
		Assigned = Assignedin;
		Group = Groupin;
		Desc = Descin;
		Frequency = Frequencyin;
		Rollover = Rolloverin;
		Status = Statusin;
		EstStartDate = EstStartDatein;
		ActStartDate = ActStartDatein;
		DaysCompInd = DaysCompIndin;
		DaysComp = DaysCompin;
		DueDate = DueDatein;
		ActCompDate = ActCompDatein;
		ExpDuration = ExpDurationin;
		ActDuration = ActDurationin;
		ReasonCodeTable = ReasonCodeTablein;
		ReasonCodeReq = ReasonCodeReqin;
		ReasonCode = ReasonCodein;
		Severity = Severityin;
		previousStatus = previousStatusin;
		Order = Orderin;
		Interval = Intervalin;
		UserTaskStatus = UserTaskStatusin;
		AutoStart = AutoStartin;
		Depend = Dependin;
		NextRecurReleased = NextRecurReleasedin;
		TaskID = TaskIDin;
		WeekDay = WeekDayin;
		MonthDay = MonthDayin;
		FreqDays = FreqDaysin;
		Closed = Closedin;
		EstHours = EstHoursin;
		SYSCRT = SYSCRTin;
		SYSUPD = SYSUPDin;
		SYSCRTBY = SYSCRTBYin;
		SYSUPDBY = SYSUPDBYin;
		PGMCRTBY = PGMCRTBYin;
		PGMUPDBY = PGMUPDBYin;
		RecentNote = RecentNotein;
		AssignedBy = AssignedByin;
		AssignedByTime = AssignedByTimein;
		StopRecurDate = StopRecurDatein;
		OriginalUser = OriginalUserin;
		OriginalUserTime = OriginalUserTimein;
		CurUser = CurUserin;
		CurUserTime = CurUserTimein;
		RecurMonth = RecurMonthin;
		SupProcType = SupProcTypein;
		SubTKNO = SubTKNOin;
		EstHours = EstHoursin;
		Cat = Catin;
		SubCat = SubCatin;
		SubCat2 = SubCat2in;
		SubCat3 = SubCat3in;
		previousStat = previousStatin;
		Mode = Modein;
	}

	public ProcessTaskRecord() {
		TSTKNO = 0;
		TSTSKNO = 0;
		TSTSKSNO = 0;
		TSTYPE = "";
		Assigned = "";
		Group = "";
		Desc = "";
		Frequency = "";
		Rollover = "";
		Status = "";
		EstStartDate = "";
		ActStartDate = "";
		DaysCompInd = "";
		DaysComp = 0;
		DueDate = "";
		ActCompDate = "";
		ExpDuration = 0;
		ActDuration = 0;
		ReasonCodeTable = "";
		ReasonCodeReq = "";
		ReasonCode = "";
		Severity = "";
		previousStatus = "";
		Order = 0;
		Interval = 0;
		UserTaskStatus = "";
		AutoStart = "";
		Depend = "";
		NextRecurReleased = "";
		TaskID = 0;
		WeekDay = "";
		MonthDay = 0;
		FreqDays = 0;
		Closed = "";
		EstHours = BigDecimal.ZERO;
		SYSCRT = "";
		SYSUPD = "";
		SYSCRTBY = "";
		SYSUPDBY = "";
		PGMCRTBY = "";
		PGMUPDBY = "";
		RecentNote = "";
		AssignedBy = "";
		AssignedByTime = "";
		StopRecurDate = "";
		OriginalUser = "";
		OriginalUserTime = "";
		CurUser = "";
		CurUserTime = "";
		RecurMonth = 0;
		SupProcType = "";
		SubTKNO = 0;
		Cat = "";
		SubCat = "";
		SubCat2 = "";
		SubCat3 = "";
		previousStat = "";
		Mode = "";
	}
}
