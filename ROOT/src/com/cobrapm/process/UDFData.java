package com.cobrapm.process;

public class UDFData {
	
	public int DTKNO;
	public int DTSKNO;
	public int DTSKSNO;   
	public int DSEQ;
	public String UNAME;
	public String UTYPE;
	public String DATA;
	public String DSVAL;
	public String Mode;
	
	public UDFData(	int DTKNOin, 
				    int DTSKNOin,
				    int DTSKSNOin,
				    int DSEQin,
				    String UNAMEin,
				    String UTYPEin,
				    String DATAin,
				    String DSVALin,
				    String Modein){
		DTKNO = DTKNOin;
		DTSKNO = DTSKNOin;
		DTSKSNO = DTSKSNOin;
		DSEQ = DSEQin;
		UNAME = UNAMEin;
		UTYPE = UTYPEin;
		DATA = DATAin;
		DSVAL = DSVALin;
		Mode = Modein;
	}

	public UDFData() {
		DTKNO = 0;
		DTSKNO = 0;
		DTSKSNO = 0;
		DSEQ = 0;
		UNAME = "";
		UTYPE = "";
		DATA = "";
		DSVAL = "";
		Mode = "";
	}
}
