package com.cobrapm.forms;

import java.util.ArrayList;


public class PFRMPROP {
	public int FPID;
	public int FPSEQ;
	public String FPNAME;
	public String FPTYPE;
	public String FPREQ;  
	public ArrayList<String> FOVALUES;
	
	public PFRMPROP(int FPIDin, 
				    int FPSEQin,
				    String FPNAMEin,
				    String FPTYPEin,
				    String FPREQin,
				    ArrayList<String> FOVALUESin){
		FPID = FPIDin;
		FPSEQ = FPSEQin;
		FPNAME = FPNAMEin;
		FPTYPE = FPTYPEin;
		FPREQ = FPREQin;
		FOVALUES = FOVALUESin;
	}

	public PFRMPROP() {
		FPID = 0;
		FPSEQ = 0;
		FPNAME = "";
		FPTYPE = "";
		FPREQ = "";
		FOVALUES = new ArrayList<String>();
	}
}
