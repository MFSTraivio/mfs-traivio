package com.cobrapm.forms;

public class PFRMEXPORT {
	public int FEID;
	public int FESEQ;
	public int FEROW;
	public int FECOL;
	
	public PFRMEXPORT(	int FEIDin, 
					    int FESEQin,
					    int FEROWin,
					    int FECOLin){
		FEID = FEIDin;
		FESEQ = FESEQin;
		FEROW = FEROWin;
		FECOL = FECOLin;
	}

	public PFRMEXPORT() {
		FEID = 0;
		FESEQ = 0;
		FEROW = 0;
		FECOL = 0;
	}
}
