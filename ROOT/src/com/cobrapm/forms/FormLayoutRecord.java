package com.cobrapm.forms;

import java.util.ArrayList;

public class FormLayoutRecord {
	public int SectionSeq;
	public int Columns;
	public ArrayList<FormDetailLayoutRecord> formLayoutDetails;
	
	public FormLayoutRecord(
					int SectionSeqin,
					int Columnsin,
					ArrayList<FormDetailLayoutRecord> formLayoutDetailsin){
		SectionSeq = SectionSeqin;
		Columns = Columnsin;
		formLayoutDetails = formLayoutDetailsin;
	}

	public FormLayoutRecord() {
		SectionSeq = 0;
		Columns = 0;
		formLayoutDetails = new ArrayList<FormDetailLayoutRecord>();
	}	
}
