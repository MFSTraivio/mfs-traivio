package com.cobrapm.authentication;

public class SpecificTaskRightsRecord {
	public boolean hasRead;
	public boolean hasWrite;
	public boolean hasDelete;
	public boolean hasPrint;
	public boolean hasExecute; 
}
