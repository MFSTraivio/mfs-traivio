package com.cobrapm.authentication;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import org.apache.log4j.Logger;



public class AuthenticationHelper {
	
	static Properties props;
	static Connection conn = null;
	private static Logger log = Logger.getLogger(AuthenticationHelper.class.getName());
	
	public boolean isStrategyAvailable(Connection conn) throws SQLException{
		String strategyAvailableYN = "";
	    	 
		log.info("...checking strategy availability");
		
	    CallableStatement stmt = null;
	    String query = "CALL SPLOCKRR03(?)";				
	    				    			    
	    try {
	        stmt = conn.prepareCall(query);
	        stmt.registerOutParameter(1, Types.VARCHAR);
	        stmt.setString(1, strategyAvailableYN);	        
	        stmt.execute();	
	        
	        strategyAvailableYN = stmt.getString(1);
	        
	        stmt.close();
	        
	    } catch (SQLException e ) {
	    	log.warn(e);
	    } finally {
	        if (stmt != null) { stmt.close(); }
	    }   
	    
        if(!strategyAvailableYN.equals("Y")){     
        	log.info("...strategy is not available");
            return(false);           
        }  
        log.info("...strategy is available");
        return(true);
        
	}
	
	public boolean isSessionValid(Connection conn, String userID, BigDecimal sessionID, BigDecimal sessionToken, BigDecimal sessionTimeout) throws SQLException{
		String isSessionValidYN = "";
		
		log.info("...checking if session is valid");
		
//		CallableStatement stmt = null;
//	    String query = "CALL SPSESSRR00(?,?,?,?,?)";	
//	    
//		try {
//	        stmt = conn.prepareCall(query);
//	        stmt.setString(1, userID);
//	        stmt.setBigDecimal(2, sessionID);
//	        stmt.setBigDecimal(3, sessionToken);
//	        stmt.setBigDecimal(4, sessionTimeout);
//	        stmt.registerOutParameter(5, Types.VARCHAR);
//	        stmt.execute();			
//	        
//	        isSessionValidYN = stmt.getString(5);
//	        
//	        stmt.close();
//	        
//	    } catch (SQLException e ) {
//	    	log.warn(e);
//	    } finally {
//	        if (stmt != null) { stmt.close(); }
//	    } 
//		
//		if(!isSessionValidYN.equals("Y")){     
//	        log.info("...session is not valid");
//	        return(false);           
//        }  
        log.info("...session is valid");
        return(true);
	}
}
