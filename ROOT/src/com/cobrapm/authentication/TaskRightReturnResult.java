package com.cobrapm.authentication;

public class TaskRightReturnResult {
    public double MODULEID; 	// Module ID
    public double TASKID; 		// Task ID  
    public double PARENTID; 	// Parent IDF3.DEFRIGHTS, 
    public double DEFRIGHTS;	// Default Rights
    public double USRRIGHTS; 	// User Rights 
    public String ISCUSTOM; 	// Custom Y/N       
    public String UDFABLE; 		// UDF-able Y/N     
    public double OVERRIDEBY; 	// Override Task ID 
    public double OVERMASK; 	// Override Mask Opt
	public boolean hasRead;
	public boolean hasWrite;
	public boolean hasDelete;
	public boolean hasPrint;
	public boolean hasExecute;    
}
