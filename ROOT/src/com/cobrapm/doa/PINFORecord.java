package com.cobrapm.doa;

public class PINFORecord {

	public String SIFTID;
	public String SIFCD;
	public String SIFSD;
	public String SIFFUL;
	public String SIFSRT;
	public int SIFSIZ;
	public String SIFRCD;
	
	public PINFORecord(	
									String SIFTIDin,
									String SIFCDin,
									String SIFSDin,
									String SIFFULin,
									String SIFSRTin,
									int SIFSIZin,
									String SIFRCDin){
		
		SIFTID = SIFTIDin;
		SIFCD = SIFCDin;
		SIFSD = SIFSDin;
		SIFFUL = SIFFULin;
		SIFSRT = SIFSRTin;
		SIFSIZ = SIFSIZin;
		SIFRCD = SIFRCDin;
	}

	public PINFORecord() {
		SIFTID = "";
		SIFCD = "";
		SIFSD = "";
		SIFFUL = "";
		SIFSRT = "";
		SIFSIZ = 0;
		SIFRCD = "";
	}
}
