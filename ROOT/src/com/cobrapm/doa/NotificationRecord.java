package com.cobrapm.doa;

public class NotificationRecord {
	public String UserID;
	public String Type;
	public NotOptionRecord optionRecord;
	
	public NotificationRecord(
					String UserIDin,
					String Typein,
					NotOptionRecord optionRecordin){
		UserID = UserIDin;
		Type = Typein;
		optionRecord = optionRecordin;
	}

	public NotificationRecord() {
		UserID = "";
		Type = "";
		optionRecord = null;
	}	
}
