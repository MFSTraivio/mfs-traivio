package com.cobrapm.doa;

public class ProcessTypeRecord {
	
	public String PORT;
	public String TKTYPE;
	public String TKTMPNM;
	
	public ProcessTypeRecord(	
									String PORTin,
									String TKTYPEin,
									String TKTMPNMin){
		
		PORT = PORTin;
		TKTYPE = TKTYPEin;
		TKTMPNM = TKTMPNMin;
	}

	public ProcessTypeRecord() {
		PORT = "";
		TKTYPE = "";
		TKTMPNM = "";
	}
}
