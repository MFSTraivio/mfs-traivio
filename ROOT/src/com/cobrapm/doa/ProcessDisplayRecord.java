package com.cobrapm.doa;

public class ProcessDisplayRecord {
	public int TKNO;
	public int TSTSKNO;
	public int TSTSKSNO;
	public String Status;
	public String Notes;
	public String ProcessName;
	public String TaskName;
	public String StartDate;
	public String EndDate;
	public String RelatedTo;
	public String Assigned;
	public String Group;
	public String FormID;
	public String SubProc;
	public String Response;
	public int Days;
	public String ProcessType;
	public String ProcessSubType;
	public String Frequency;
	public String Category;
	public String SubCategory;
	public String SubCategory2;
	public String SubCategory3;
	public String Severity;
	public double Amount;
	public String Approved;
	public String CaseNumber;
	public double JobNumber;
	public String HardDeadline;
	public String Type;
	public int Estimate;
	public int rownumber;
	public boolean isSelected;
	public int NoteCount;
	
	public ProcessDisplayRecord(	int TKNOin,
									int TSTSKNOin,
									int TSTSKSNOin,
									String Statusin,
									String Notesin,
									String ProcessNamein,
									String TaskNamein,
									String StartDatein,
									String EndDatein,
									String RelatedToin,
									String Assignedin,
									String FormIDin,
									String SubProcin,
									String Responsein,
									int Daysin,
									String ProcessTypein,
									String ProcessSubTypein,
									String Frequencyin,
									String Categoryin,
									String SubCategoryin,
									String SubCategory2in,
									String SubCategory3in,
									String Severityin,
									double Amountin,
									String Approvedin,
									String CaseNumberin,
									double JobNumberin,
									String HardDeadlinein,
									String Typein,
									int Estimatein,
									int RowNumberin,
									boolean isSelectedin,
									int NoteCountin){
		
		TKNO = TKNOin;
		TSTSKNO = TSTSKNOin;
		TSTSKSNO = TSTSKSNOin;
		FormID = FormIDin;
		SubProc = SubProcin;
		Response = Responsein;
		Status = Statusin;
		Notes = Notesin;
		ProcessName = ProcessNamein;
		TaskName = TaskNamein;
		StartDate = StartDatein;
		EndDate = EndDatein;
		RelatedTo = RelatedToin;
		Assigned = Assignedin;
		Days = Daysin;
		ProcessType = ProcessTypein;
		ProcessSubType = ProcessSubTypein;
		Frequency = Frequencyin;
		Category = Categoryin;
		SubCategory = SubCategoryin;
		SubCategory2 = SubCategory2in;
		SubCategory3 = SubCategory3in;
		Severity = Severityin;
		Amount = Amountin;
		Approved = Approvedin;
		CaseNumber = CaseNumberin;
		JobNumber = JobNumberin;
		HardDeadline = HardDeadlinein;
		Type = Typein;
		Estimate = Estimatein;
		rownumber = RowNumberin;
		isSelected = isSelectedin;
		NoteCount = NoteCountin;
	}

	public ProcessDisplayRecord() {
		TKNO = 0;
		TSTSKNO = 0;
		TSTSKSNO = 0;
		FormID = "";
		SubProc = "";
		Response = "";
		Status = "";
		Notes = "";
		ProcessName = "";
		TaskName = "";
		StartDate = "";
		EndDate = "";
		RelatedTo = "";
		Assigned = "";
		Days = 0;
		ProcessType = "";
		ProcessSubType = "";
		Frequency = "";
		Category = "";
		SubCategory = "";
		SubCategory2 = "";
		SubCategory3 = "";
		Severity = "";
		Amount = 0;
		Approved = "";
		CaseNumber = "";
		JobNumber = 0;
		HardDeadline = "";
		Type = "";
		Estimate = 0;
		rownumber = 0;
		isSelected = false;
		NoteCount = 0;
	}
}
