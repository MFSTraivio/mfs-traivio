package com.cobrapm.doa;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"checked"})

public class DefinedFieldsRecord {
	public int DFSEQ;
	public String DFNAMID;
	public String DFDISP;
	public String DFREQ;
	public String DFWIDTH;;
	
	public DefinedFieldsRecord(int DFSEQin,
					String DFNAMIDin,
					String DFDISPin,
					String DFREQin,
					String DFWIDTHin){
		DFSEQ = DFSEQin;
		DFNAMID = DFNAMIDin;
		DFDISP = DFDISPin;
		DFREQ = DFREQin;
		DFWIDTH = DFWIDTHin;
	}

	public DefinedFieldsRecord() {
		DFSEQ = 0;
		DFNAMID = "";
		DFDISP = "";
		DFREQ = "";
		DFWIDTH = "";
	}	
}
