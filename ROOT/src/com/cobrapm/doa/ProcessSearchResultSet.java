package com.cobrapm.doa;

public class ProcessSearchResultSet {
	public int SESSIONID;
	public int WIDGETID;
	public String RECTYPE;
	public int TKNO;
	public int TSTSKNO;
	public int TSTSKSNO;
	public String TSTYPED;
	public String TKTMPNM;
	public int TSPRID;
	public String TSPRID_SIFSD;
	public String TSPRID_SIFFUL;
	public String TKQUEUE;
	public String TKESTST;
	public String TKESTCMP;
	public String TKSTAT;
	public String TKSTAT_SIFSD;
	public String TKSTAT_SIFFUL;
	public String TKASSIGN;
	public String KAJOBROLE;
	public String SYSUPD;
	public String SYSUPDBY;
	public String TKTYPE;
	public String TKSUBTYP;
	public String TKSUBTYP_SIFSD;
	public String TKSUBTYP_SIFFUL;
	public int TKPRID;
	public String TKPRID_SIFSD;
	public String TKPRID_SIFFUL;
	public String TKTYPSD;
	public String TKFREQ;
	public String TKFREQ_SIFSD;
	public String TKFREQ_SIFFUL;
	
	public String T4TFREQ;
	public String T4TFREQSD;
    public String T4TFREQFD;
    public String T4TWGTYPE;
    public int T4TTASKID;
    public int T4BDAYSDUE;
    public String T4FULLJD;
        
    public String T4BNOTE600;
    public String T4BNOTEMF;
    public String T4BDISPLY;
    public String T4BTYPED;
    public String T4PASSIGNL;
    public String T4BQDESC;
    public String T4BUSTAT;
    public int T4BOTDAYS;
    public int T4BWNDAYS;
    public String T4BACTST;
    public String T4BACTCMP;
    public String T4BCAT;
    public String T4BSUBCAT;
    public String T4BSUBCAT2;
    public String T4BSUBCAT3;
    public String T4BCATD;
    public String T4BSUBCATD;
    public String T4BSUBCA2D;
    public String T4BSUBCA3D;
    public String T4PQUEUE;
    public String T4PQUEUED;
    public String T4BNTEFLG;
    public String T4BNTEPATH;
    public String T4BNTEFNM;
    public String  T4BNTEFATN;
    public double T4ESTHRS;
	
	public ProcessSearchResultSet(	int SESSIONIDin,
									int WIDGETIDin,
									String RECTYPEin,
									int TKNOin,
									int TSTSKNOin,
									int TSTSKSNOin,
									String TSTYPEDin,
									String TKTMPNMin,
									int TSPRIDin,
									String TSPRID_SIFSDin,
									String TSPRID_SIFFULin,
									String TKQUEUEin,
									String TKESTSTin,
									String TKESTCMPin,
									String TKSTATin,
									String TKSTAT_SIFSDin,
									String TKSTAT_SIFFULin,
									String TKASSIGNin,
									String KAJOBROLEin,
									String SYSUPDin,
									String SYSUPDBYin,
									String TKTYPEin,
									String TKSUBTYPin,
									String TKSUBTYP_SIFSDin,
									String TKSUBTYP_SIFFULin,
									int TKPRIDin,
									String TKPRID_SIFSDin,
									String TKPRID_SIFFULin,
									String TKTYPSDin,
									String TKFREQin,
									String TKFREQ_SIFSDin,
									String TKFREQ_SIFFULin,									
									String T4TFREQin,
									String T4TFREQSDin,
								    String T4TFREQFDin,
								    String T4TWGTYPEin,
								    int T4TTASKIDin,
								    int T4BDAYSDUEin,
								    String T4FULLJDin,								        
								    String T4BNOTE600in,
								    String T4BNOTEMFin,
								    String T4BDISPLYin,
								    String T4BTYPEDin,
								    String T4PASSIGNLin,
								    String T4BQDESCin,
								    String T4BUSTATin,
								    int T4BOTDAYSin,
								    int T4BWNDAYSin,
								    String T4BACTSTin,
								    String T4BACTCMPin,
								    String T4BCATin,
								    String T4BSUBCATin,
								    String T4BSUBCAT2in,
								    String T4BSUBCAT3in,
								    String T4BCATDin,
								    String T4BSUBCATDin,
								    String T4BSUBCA2Din,
								    String T4BSUBCA3Din,
								    String T4PQUEUEin,
								    String T4PQUEUEDin,
								    String T4BNTEFLGin,
								    String T4BNTEPATHin,
								    String T4BNTEFNMin,
								    String  T4BNTEFATNin,
								    double T4ESTHRSin){
		SESSIONID = SESSIONIDin;
		WIDGETID = WIDGETIDin;
		RECTYPE = RECTYPEin;
		TKNO = TKNOin;
		TSTSKNO = TSTSKNOin;
		TSTSKSNO = TSTSKSNOin;
		TSTYPED = TSTYPEDin;
		TKTMPNM = TKTMPNMin;
		TSPRID = TSPRIDin;
		TSPRID_SIFSD = TSPRID_SIFSDin;
		TSPRID_SIFFUL = TSPRID_SIFFULin;
		TKQUEUE = TKQUEUEin;
		TKESTST = TKESTSTin;
		TKESTCMP = TKESTCMPin;
		TKSTAT = TKSTATin;
		TKSTAT_SIFSD = TKSTAT_SIFSDin;
		TKSTAT_SIFFUL = TKSTAT_SIFFULin;
		TKASSIGN = TKASSIGNin;
		KAJOBROLE = KAJOBROLEin;
		SYSUPD = SYSUPDin;
		SYSUPDBY = SYSUPDBYin;
		TKTYPE = TKTYPEin;
		TKSUBTYP = TKSUBTYPin;
		TKSUBTYP_SIFSD = TKSUBTYP_SIFSDin;
		TKSUBTYP_SIFFUL = TKSUBTYP_SIFFULin;
		TKPRID = TKPRIDin;
		TKPRID_SIFSD = TKPRID_SIFSDin;
		TKPRID_SIFFUL = TKPRID_SIFFULin;
		TKTYPSD = TKTYPSDin;
		TKFREQ = TKFREQin;
		TKFREQ_SIFSD = TKFREQ_SIFSDin;
		TKFREQ_SIFFUL = TKFREQ_SIFFULin;									
		T4TFREQ = T4TFREQin;
		T4TFREQSD = T4TFREQSDin;
		T4TFREQFD = T4TFREQFDin;
		T4TWGTYPE = T4TWGTYPEin;
		T4TTASKID = T4TTASKIDin;
		T4BDAYSDUE = T4BDAYSDUEin;
		T4FULLJD = T4FULLJDin;								        
		T4BNOTE600 = T4BNOTE600in;
		T4BNOTEMF = T4BNOTEMFin;
		T4BDISPLY = T4BDISPLYin;
		T4BTYPED = T4BTYPEDin;
		T4PASSIGNL = T4PASSIGNLin;
		T4BQDESC = T4BQDESCin;
		T4BUSTAT = T4BUSTATin;
		T4BOTDAYS = T4BOTDAYSin;
		T4BWNDAYS = T4BWNDAYSin;
		T4BACTST = T4BACTSTin;
		T4BACTCMP = T4BACTCMPin;
		T4BCAT = T4BCATin;
		T4BSUBCAT = T4BSUBCATin;
		T4BSUBCAT2 = T4BSUBCAT2in;
		T4BSUBCAT3 = T4BSUBCAT3in;
		T4BCATD = T4BCATDin;
		T4BSUBCATD = T4BSUBCATDin;
		T4BSUBCA2D = T4BSUBCA2Din;
		T4BSUBCA3D = T4BSUBCA3Din;
		T4PQUEUE = T4PQUEUEin;
		T4PQUEUED = T4PQUEUEDin;
		T4BNTEFLG = T4BNTEFLGin;
		T4BNTEPATH = T4BNTEPATHin;
		T4BNTEFNM = T4BNTEFNMin;
		T4BNTEFATN = T4BNTEFATNin;
		T4ESTHRS = T4ESTHRSin;
	}

	public ProcessSearchResultSet() {
		SESSIONID = 0;
		WIDGETID = 0;
		RECTYPE = "";
		TKNO = 0;
		TSTSKNO = 0;
		TSTSKSNO = 0;
		TSTYPED = "";
		TKTMPNM = "";
		TSPRID = 0;
		TSPRID_SIFSD = "";
		TSPRID_SIFFUL = "";
		TKQUEUE = "";
		TKESTST = "";
		TKESTCMP = "";
		TKSTAT = "";
		TKSTAT_SIFSD = "";
		TKSTAT_SIFFUL = "";
		TKASSIGN = "";
		KAJOBROLE = "";
		SYSUPD = "";
		SYSUPDBY = "";
		TKTYPE = "";
		TKSUBTYP = "";
		TKSUBTYP_SIFSD = "";
		TKSUBTYP_SIFFUL = "";
		TKPRID = 0;
		TKPRID_SIFSD = "";
		TKPRID_SIFFUL = "";
		TKTYPSD = "";
		TKFREQ = "";
		TKFREQ_SIFSD = "";
		TKFREQ_SIFFUL = "";									
		T4TFREQ = "";
		T4TFREQSD = "";
		T4TFREQFD = "";
		T4TWGTYPE = "";
		T4TTASKID = 0;
		T4BDAYSDUE = 0;
		T4FULLJD = "";								        
		T4BNOTE600 = "";
		T4BNOTEMF = "";
		T4BDISPLY = "";
		T4BTYPED = "";
		T4PASSIGNL = "";
		T4BQDESC = "";
		T4BUSTAT = "";
		T4BOTDAYS = 0;
		T4BWNDAYS = 0;
		T4BACTST = "";
		T4BACTCMP = "";
		T4BCAT = "";
		T4BSUBCAT = "";
		T4BSUBCAT2 = "";
		T4BSUBCAT3 = "";
		T4BCATD = "";
		T4BSUBCATD = "";
		T4BSUBCA2D = "";
		T4BSUBCA3D = "";
		T4PQUEUE = "";
		T4PQUEUED = "";
		T4BNTEFLG = "";
		T4BNTEPATH = "";
		T4BNTEFNM = "";
		T4BNTEFATN = "";
		T4ESTHRS = 0;
	}
}
