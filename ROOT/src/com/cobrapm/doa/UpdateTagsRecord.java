package com.cobrapm.doa;

import java.util.ArrayList;

import org.codehaus.jackson.type.TypeReference;


public class UpdateTagsRecord {
	public String lib;
	public String userid;
	public int noteID;
	public ArrayList<PRELDESC> tags;

	public UpdateTagsRecord(	String libin,
								String useridin,
								int noteIDin,
								ArrayList<PRELDESC> tagsIn){
		
		lib = libin;
		userid = useridin;
		noteID = noteIDin;
		tags = tagsIn;
	}

	public UpdateTagsRecord() {
		lib = "";
		userid = "";
		noteID = 0;
		tags = null;
	}
}
