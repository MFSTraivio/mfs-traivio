package com.cobrapm.doa;

public class UserDisplayRecord {
	public int DSSEQ;
	public int DSORDER;
	public String DSTYPE;
	public int DSSEQNUM;
	public String NAMID;
	public String WIDTH;
	public String sort;
	public String sortfunc;
	public String style;
	public String colstyle;
	
	public UserDisplayRecord(int DSSEQin,
					int DSORDERin,
					String DSTYPEin,
					int DSSEQNUMin,
					String NAMIDin,
					String WIDTHin,
					String sortin,
					String sortfuncin,
					String stylein,
					String colstylein){
		DSSEQ = DSSEQin;
		DSORDER = DSORDERin;
		DSTYPE = DSTYPEin;
		DSSEQNUM = DSSEQNUMin;
		NAMID = NAMIDin;
		WIDTH = WIDTHin;
		sort = sortin;
		sortfunc = sortfuncin;
		style = stylein;
		colstyle = colstylein;
	}

	public UserDisplayRecord() {
		DSSEQ = 0;
		DSORDER = 0;
		DSTYPE = "";
		DSSEQNUM = 0;
		NAMID = "";
		WIDTH = "";
		sort = "";
		sortfunc = "";
		style = "cursor:pointer; text-align:center !important; color:black;";
		colstyle = "cursor:pointer; text-align:center !important; color:black;";
	}	
}
