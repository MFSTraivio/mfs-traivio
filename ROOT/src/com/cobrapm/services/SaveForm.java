package com.cobrapm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.forms.PFRMEXPORT;
import com.cobrapm.forms.PFRMMSTR;
import com.cobrapm.forms.PFRMPROP;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.IFSFileOutputStream;
   
@Path("SaveForm")  
public class SaveForm { 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	//private AuthenticationHelper authenticate = new AuthenticationHelper();
	private static Logger log = Logger.getLogger(SaveForm.class.getName());
	
	@POST 
	@Path("/save")  
    @Produces(MediaType.APPLICATION_JSON)
    public String saveResponse(String masterString) throws IOException {  

   	CONNECTOR CONNECTOR = new CONNECTOR(); 
   	Connection conn = null;
   	
   	String json = "";
	
   	String responses = "";
   	PFRMMSTR masterRecord = new PFRMMSTR();
   	ArrayList<PFRMPROP> PFRMPROPs = new ArrayList<PFRMPROP>();
//   	Encrypt encrypt = new Encrypt();
   	
   	log.info("..mapping JSON string");
   	
   	JsonElement jelement = new JsonParser().parse(masterString);
    JsonObject  jobject = jelement.getAsJsonObject();
    
    String lib = jobject.get("lib").toString();
    lib = lib.substring(1, lib.length()-1);
    
    String user = jobject.get("userid").toString();
    user = user.substring(1, user.length()-1);
    
    String pgm = "SFRMMSRU00";
    
    masterRecord.FRID = jobject.get("formID").getAsInt();
    masterRecord.FRNAME = jobject.get("FormName").toString();
    masterRecord.FRNAME = masterRecord.FRNAME.substring(1, masterRecord.FRNAME.length()-1);
    masterRecord.FREXTNL = jobject.get("External").toString();
    masterRecord.FREXTNL = masterRecord.FREXTNL.substring(1, masterRecord.FREXTNL.length()-1);
    masterRecord.FREXTHSH = jobject.get("ExternalHash").toString();
    masterRecord.FREXTHSH = masterRecord.FREXTHSH.substring(1, masterRecord.FREXTHSH.length()-1);
    try{
    	masterRecord.FRPORT = jobject.get("PorT").toString();
    	masterRecord.FRPORT = masterRecord.FRPORT.substring(1, masterRecord.FRPORT.length()-1);
    	masterRecord.FRPORTID = jobject.get("PorTID").toString();
    	masterRecord.FRPORTID = masterRecord.FRPORTID.substring(1, masterRecord.FRPORTID.length()-1);
    	masterRecord.FRPORTLBL = jobject.get("PorTLBL").getAsInt();
    	masterRecord.FRPASSIGN = jobject.get("PAssign").getAsBoolean();
    } catch (NullPointerException e){
    	
    }
    responses = jobject.get("responses").toString();
   	
   	ObjectMapper mapper = new ObjectMapper();
   	
   	try {
   		PFRMPROPs = mapper.readValue(responses, new TypeReference<ArrayList<PFRMPROP>>(){});
	} catch (JsonParseException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (JsonMappingException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
   	
   	if(masterRecord.FRID == 0 || (masterRecord.FREXTNL.equals("Y") && masterRecord.FRID != 0 && masterRecord.FREXTHSH.equals(""))){
   		SecureRandom random = new SecureRandom();
   		masterRecord.FREXTHSH = new BigInteger(130, random).toString(32);
   	}
   	
//   	lib = encrypt.decryptLibrary(lib);
   	
   	if(PFRMPROPs.size() > 0){
   		String mode = "I";
   		if(masterRecord.FRID != 0){
   			mode = "U";
   		}
	  		try
			{									
	  			CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
	  			
				log.info("..adding master record");
				
				CallableStatement stmt = null;
			    String query = "CALL SFRMMSRU00(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			    				    			    
			    try {
			    	stmt = conn.prepareCall(query);
			    	stmt.registerOutParameter(1, Types.INTEGER);
			    	stmt.registerOutParameter(14, Types.VARCHAR);
			    	stmt.setInt(1, masterRecord.FRID);
			        stmt.setString(2, masterRecord.FRNAME);
			        stmt.setString(3, masterRecord.FREXTNL);
			        stmt.setString(4, masterRecord.FREXTHSH);
			        stmt.setString(5, masterRecord.FRPORT);
			        stmt.setString(6, masterRecord.FRPORTID);
			        stmt.setInt(7, masterRecord.FRPORTLBL);
			        if(masterRecord.FRPASSIGN == false){
			        	stmt.setString(8, "N");
			        } else {
			        	stmt.setString(8, "Y");
			        }
			        stmt.setString(9, mode);
			        stmt.setString(10, user);
			        stmt.setString(11, user);
			        stmt.setString(12, pgm);
			        stmt.setString(13, pgm);
			        stmt.executeUpdate();	
			        
			        int FRID = 0;
			        if(mode.equals("I")){
			        	FRID = stmt.getInt(1);
			        } else{
			        	FRID = masterRecord.FRID;
			        }
			    	
			        for(int i=0; i<PFRMPROPs.size(); i++){
			        	PFRMPROPs.get(i).FPID = FRID;
			        }
			               			        
			        stmt.close();
			        
			    } catch (SQLException e ) {
			    	log.warn(e.getMessage());
			    	json = new Gson().toJson(e.getMessage());
			    	return (json); 
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
						
				log.info("..added master record. Adding children");
								
				stmt = null;
			    query = "CALL SFRMPRRU00(?,?,?,?,?,?,?,?,?,?)";	
			    pgm = "SFRMPRRU00";
			    				    			    
			    try {
			    	for(int i=0; i<PFRMPROPs.size(); i++){
				        stmt = conn.prepareCall(query);
				        stmt.registerOutParameter(2, Types.INTEGER);
				        stmt.registerOutParameter(10, Types.VARCHAR);
				        stmt.setInt(1, PFRMPROPs.get(i).FPID);
				        stmt.setString(3, PFRMPROPs.get(i).FPNAME);
				        stmt.setString(4, PFRMPROPs.get(i).FPTYPE);
				        stmt.setString(5, PFRMPROPs.get(i).FPREQ);	
				        stmt.setString(6, user);	
				        stmt.setString(7, user);	
				        stmt.setString(8, pgm);	
				        stmt.setString(9, pgm);	
				        stmt.executeUpdate();			        
				        
				        int FRID = stmt.getInt(2);	
				        if(PFRMPROPs.get(i).FOVALUES.size() > 0){
				        	log.info("..adding options");
				        	CallableStatement substmt = null;
						    String subquery = "CALL SFRMPORU00(?,?,?,?,?)";	
					        for(int n=0; n<PFRMPROPs.get(i).FOVALUES.size(); n++){
					        	int FOOSEQ = 0;
					        	substmt = conn.prepareCall(subquery);
					        	substmt.registerOutParameter(5, Types.VARCHAR);
					        	substmt.setInt(1, PFRMPROPs.get(i).FPID);
					        	substmt.setInt(2, FRID);
					        	substmt.setInt(3, FOOSEQ);
					        	substmt.setString(4, PFRMPROPs.get(i).FOVALUES.get(n));				        
					        	substmt.executeUpdate();									    
					        }
					        substmt.close();
					        log.info("..added options");
				        }
				        
				        stmt.close();
			    	}		     		        
			    } catch (SQLException e) {
			    	log.warn(e.getMessage());
			    	json = new Gson().toJson(e.getMessage());
			    	return (json); 
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
	  		
				log.info("..Added children");
					 
				connectionHelper.closeConnection(conn);								
				
			} catch(Exception ex) {
				log.warn(ex);
				connectionHelper.closeConnection(conn);
			}  		  		
 				
   	}
   	json = new Gson().toJson("Success");	
   	return (json); 
     } 
	 
	 @GET 
	 @Path("/saveExport/{lib}/{formid}/{user}/{sessionID}/{sessionToken}/{sessionTimeout}/{responses}")  
     @Produces(MediaType.APPLICATION_JSON)
     public Response saveExport(@PathParam("lib") String lib, @PathParam("formid") String formid, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout, @PathParam("responses") String responses) throws IOException {  

    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
 	
    	ArrayList<PFRMEXPORT> PFRMEXPORTS = new ArrayList<PFRMEXPORT>();
    	
    	log.info("..mapping JSON string");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	PFRMEXPORTS = mapper.readValue(responses, new TypeReference<ArrayList<PFRMEXPORT>>(){});
//    	Encrypt encrypt = new Encrypt();
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
    	if(PFRMEXPORTS.size() > 0){
	  		try
			{									
	  			CONNECTOR = connectionHelper.getConnection(lib);
	  			conn = CONNECTOR.conn;
	  			
				log.info("..adding master record");
				
				CallableStatement stmt = null;
			    String query = "CALL SFRMEXRU00(?,?,?,?,?,?)";				
			    
			    stmt = conn.prepareCall(query);
		    	stmt.registerOutParameter(6, Types.VARCHAR);
		        stmt.setInt(1, PFRMEXPORTS.get(1).FEID);
		        stmt.setInt(2, PFRMEXPORTS.get(1).FESEQ);
		        stmt.setInt(3, PFRMEXPORTS.get(1).FEROW);
		        stmt.setInt(4, PFRMEXPORTS.get(1).FECOL);	
		        stmt.setString(5, "D");
		        stmt.executeUpdate();					               			        
		        stmt.close();
			    
			    for(int i=0; i<PFRMEXPORTS.size(); i++){
				    try {
				    	stmt = conn.prepareCall(query);
				    	stmt.registerOutParameter(6, Types.VARCHAR);
				        stmt.setInt(1, PFRMEXPORTS.get(i).FEID);
				        stmt.setInt(2, PFRMEXPORTS.get(i).FESEQ);
				        stmt.setInt(3, PFRMEXPORTS.get(i).FEROW);
				        stmt.setInt(4, PFRMEXPORTS.get(i).FECOL);				        
				        stmt.setString(5, "I");
				        stmt.executeUpdate();					               			        
				        stmt.close();
				        
				    } catch (SQLException e ) {
				    	log.warn(e.getMessage());
				    	return Response.status(500).entity(e.getMessage()).build(); 
				    } finally {
				        if (stmt != null) { stmt.close(); }
				    }
			    }
			    
				log.info("..added export records");
				String directory = lib + "_" + formid;
				
				SaveFormResponse SaveFormResponse = new SaveFormResponse();
				
				String fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
				fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));		        		
				fullpath = fullpath + File.separator + "templates" + File.separator + directory;	
												
				File dir = new File(fullpath);
				File[] directoryListing = dir.listFiles();
				if (directoryListing != null) {					  					
					  
					// Create an AS400 object.  This new directory will 
			        // be created on this system.
					Context initialContext = (Context)new InitialContext().lookup("java:comp/env");
				    String dbhost = (String)initialContext.lookup("dbhost");  					  
					String userid = (String)initialContext.lookup("dbuser");  
					String password = (String)initialContext.lookup("dbpassword");  
					  
			        AS400 sys = new AS400(dbhost, userid, password);
			        
			        // /$Library
			        String directorypath = lib;  
			        SaveFormResponse.createDirectory(sys, directorypath);
			        
			        // /$Library/Forms
			        directorypath = lib + IFSFile.separator + "Forms";  
			        SaveFormResponse.createDirectory(sys, directorypath);
			        
			        // /$Library/Forms/$Formid
			        directorypath = lib + IFSFile.separator + "Forms" + IFSFile.separator + formid;  
			        SaveFormResponse.createDirectory(sys, directorypath);
				        
			        // /$Library/Forms/$Formid/$thisresponsenumber
			        directorypath = lib + IFSFile.separator + "Forms" + IFSFile.separator + formid + IFSFile.separator + "Export";  
			        SaveFormResponse.createDirectory(sys, directorypath);
			       
			        IFSFile IFSdir = new IFSFile(sys, directorypath);
			        IFSFile[] IFSdirectoryListing = IFSdir.listFiles();
			        for (IFSFile child : IFSdirectoryListing) {
			        	boolean success = child.delete();
			        	if(success){
			        		log.info("..template deleted");
			        	} else {
			        		log.info("..template not deleted");
			        	}	
			        }
			        
				    for (File child : directoryListing) {
				        // Create a file object that represents the directory.
				    	String filename = child.getName(); 
						FileInputStream sourceStream = new FileInputStream(fullpath + IFSFile.separator + filename);
				        IFSFileOutputStream destination = new IFSFileOutputStream(sys,  directorypath + IFSFile.separator + filename);
				        byte[] buffer = new byte[(int)child.length()];
				        sourceStream.read(buffer);
				        destination.write(buffer);
				   	 	destination.close(); 
				   	 	sourceStream.close();
				    }
	
				    FileUtils.deleteDirectory(dir);				    
				    sys.disconnectAllServices();
				    				    
				}				 				  
				
				connectionHelper.closeConnection(conn);								
				
			} catch(Exception ex) {
				log.warn(ex);
				connectionHelper.closeConnection(conn);
			}  		  		
  				
    	}
    	return Response.status(200).entity("success").build(); 
      } 
  		
}  