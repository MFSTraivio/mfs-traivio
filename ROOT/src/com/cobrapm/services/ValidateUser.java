package com.cobrapm.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.cobrapm.authentication.AuthenticationHelper;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.Encrypt;
import com.cobrapm.forms.FormDetailLayoutRecord;
import com.cobrapm.forms.FormLayoutRecord;
import com.cobrapm.forms.PFRMMSTR;
import com.cobrapm.forms.PFRMPROP;
import com.cobrapm.forms.PFRMRESM;
import com.cobrapm.forms.PFRMRESP;
   
@Path("ValidateUser")  
public class ValidateUser { 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private AuthenticationHelper authenticate = new AuthenticationHelper();
	private static Logger log = Logger.getLogger(ValidateUser.class.getName());
	
     @GET  
     @Path("/validate/{datasource}/{userid}/{sessionID}/{sessionToken}/{sessionTimeout}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String validate(@PathParam("datasource") String lib, @PathParam("userid") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	String returnString = "{\"VALID\": \"User is Valid\"}";
//    	Encrypt encrypt = new Encrypt();
    	
//    	lib = encrypt.decryptLibrary(lib);
    	try{
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn; 		    			
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  	
    	return returnString;
      }
     
     @GET  
     @Path("/validateForm/{datasource}/{formid}/{formhash}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String validateForm(@PathParam("datasource") String libEncrypted, @PathParam("formid") int formid, 
    		 @PathParam("formhash") String formhash) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
    	
    	PFRMMSTR PFRMMSTR = new PFRMMSTR();
    	PFRMPROP PFRMPROP = new PFRMPROP();
    	ArrayList<PFRMPROP> PFRMPROPs = new ArrayList<PFRMPROP>();
    	PFRMRESP PFRMRESP = new PFRMRESP();
    	ArrayList<PFRMRESP> PFRMRESPs = new ArrayList<PFRMRESP>();
    	PFRMRESM PFRMRESM = new PFRMRESM();
    	ArrayList<FormLayoutRecord> layoutRecords = new ArrayList<FormLayoutRecord>();
    	
    	String json = "";
    	
    	try{
    		Encrypt decryptLibrary = new Encrypt();
    		String lib = decryptLibrary.decryptLibrary(libEncrypted);
    		CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn; 
  			
  			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SFRMMSCR00(?)";				
		    				    			    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMMSTR.FRID = rs.getInt(1);	
		        	PFRMMSTR.FRNAME = rs.getString(2).trim();
		        	PFRMMSTR.FREXTNL = rs.getString(3).trim();
		        	PFRMMSTR.FREXTHSH = rs.getString(4).trim();
		        }
		        
		        rs.close();
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
			
		    if(PFRMMSTR.FRID == 0){
		    	return "{\"ERROR\": \"Form does not exist\"}";
		    }	   
		    
			log.info("..retrieved record. retrieving detail records");
							
			stmt = null;
		    query = "CALL SFRMPRCR00(?)";				
		    				    			    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMPROP = new PFRMPROP();
		        	PFRMPROP.FPID = rs.getInt(1);	
		        	PFRMPROP.FPSEQ = rs.getInt(2);
		        	PFRMPROP.FPNAME = rs.getString(3).trim();
		        	PFRMPROP.FPTYPE = rs.getString(4).trim();
		        	PFRMPROP.FPREQ = rs.getString(5).trim();
		        	PFRMPROPs.add(PFRMPROP);
		        }		        		        
		        
		        rs.close();
		        stmt.close();
		        		     		        
		    } catch (SQLException e) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
					
			log.info("..retrieved " + PFRMPROPs.size() + " detail records. Retreiving field options.");
									
			try {
				for(int i=0; i<PFRMPROPs.size(); i++){
					stmt = null;
				    query = "CALL SFRMPOCR00(?,?)";		
				    
			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			        stmt.setInt(1,  PFRMPROPs.get(i).FPID);
			        stmt.setInt(2,  PFRMPROPs.get(i).FPSEQ);
			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while (rs.next()) {
			        	String reciever = rs.getString(1).trim();
			        	PFRMPROPs.get(i).FOVALUES.add(reciever);
			        }
			        
			        rs.close();
			        stmt.close();
			        
				}	        		        		     		        
		    } catch (SQLException e) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
			
			log.info("..Retreiving layout Data.");
			
			try {
				stmt = null;
			    query = "CALL SFLSECCR00(?)";		
			    	
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setInt(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	FormLayoutRecord tmpRecord = new FormLayoutRecord();
		        	tmpRecord.SectionSeq = rs.getInt(1);	
		        	tmpRecord.Columns = rs.getInt(2);
		        	layoutRecords.add(tmpRecord);
		        }
		        
		        rs.close();
		        stmt.close();
			                		        		     		        
		    } catch (SQLException e) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
			
			for(FormLayoutRecord tmpRecord : layoutRecords){
				try {
					stmt = null;
					query = "CALL SFLSECCR01(?,?)";		
			    
					stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
					stmt.setInt(1, formid);
					stmt.setInt(2, tmpRecord.SectionSeq);
					stmt.execute();			        
		        
					ResultSet rs = stmt.getResultSet();
					while (rs.next()) {
						FormDetailLayoutRecord tmpRecord2 = new FormDetailLayoutRecord();
						tmpRecord2.ColumnSeq = rs.getInt(1);	
						tmpRecord2.LayoutSeq = rs.getInt(2);
						tmpRecord2.PropSeq = rs.getInt(3);
						tmpRecord.formLayoutDetails.add(tmpRecord2);
					}
		        
					rs.close();
					stmt.close();
			                		        		     		        
				} catch (SQLException e) {
					log.warn(e);
				} finally {
					if (stmt != null) { stmt.close(); }
				}
			}
				
			PFRMMSTR.PFRMPROP = PFRMPROPs;
			PFRMMSTR.layouts = layoutRecords;
			PFRMRESM.PFRMMSTR = PFRMMSTR;
			
			log.info("..retrieved field options.");
				 
			connectionHelper.closeConnection(conn);
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	  		json = ow.writeValueAsString(PFRMRESM);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  	
    	return json;
      }
     
     @GET  
     @Path("/encryptLib/{datasource}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String encryptLib(@PathParam("datasource") String lib) throws IOException {  
    	
    	 Encrypt encryptLibrary = new Encrypt();
    	 String returnString = encryptLibrary.encryptLibrary(lib);
    	
    	return returnString;
      }
      		
}  
