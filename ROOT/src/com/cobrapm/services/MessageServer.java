package com.cobrapm.services;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cobrapm.websocket.MessageMediator;

@Path("MessageServer")  
public class MessageServer {
	
	private MessageMediator client;
//	private final String webSocketAddress = "ws://localhost:8080/ROOT/messages";
	private final String webSocketAddress = "ws://mfs.traiv.io/messages";
	
	private static Logger log = Logger.getLogger(MessageServer.class.getName());
	
	public MessageServer() {
	}
	 
	private void initializeWebSocket() throws URISyntaxException {
		//ws://localhost:7101/CinemaMonitor/cinemaSocket/
		System.out.println("REST service: open websocket client at " + webSocketAddress);
		client = new MessageMediator(new URI(webSocketAddress));
	}
	 
	private void sendMessageOverSocket(String message) {
		if (client == null) {
			try {
				initializeWebSocket();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		client.sendMessage(message);
	 
	}
	
	@GET  
    @Path("/getMessage/{type}")  
    @Produces(MediaType.APPLICATION_JSON)
    public String getMessage(@PathParam("type") String type) throws IOException {
		sendMessageOverSocket(type);
		return type;
    }

}
