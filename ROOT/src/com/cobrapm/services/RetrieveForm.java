package com.cobrapm.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.type.TypeReference;

import com.cobrapm.authentication.AuthenticationHelper;
import com.cobrapm.authentication.AuthorizationRecord;
import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.doa.PRELDESC;
import com.cobrapm.doa.ProcessTypeRecord;
import com.cobrapm.forms.PFRMMSTR;
import com.cobrapm.forms.PFRMPROP;
import com.cobrapm.notes.PNOTE2ATTRec;
import com.cobrapm.notes.PNOTE2REL;
import com.cobrapm.notes.PNOTE2Rec;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
   
@Path("RetrieveForm")  
public class RetrieveForm { 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	private AuthenticationHelper authenticate = new AuthenticationHelper();
	private static Logger log = Logger.getLogger(RetrieveForm.class.getName());
	
     @GET  
     @Path("/accessForm/{lib}/{formid}/{user}/{sessionID}/{sessionToken}/{sessionTimeout}")  
     @Produces(MediaType.APPLICATION_JSON)
     public String accessForm(@PathParam("lib") String lib, @PathParam("formid") String formid, @PathParam("user") String user, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout) throws IOException {  
    	 
    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
 	
    	PFRMMSTR PFRMMSTR = new PFRMMSTR();
    	PFRMPROP PFRMPROP = new PFRMPROP();
    	ArrayList<PFRMPROP> PFRMPROPs = new ArrayList<PFRMPROP>();
//    	Encrypt encrypt = new Encrypt();
    	
    	String json = "";
    	
//    	lib = encrypt.decryptLibrary(lib);
  		try
		{	
  			CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SFRMMSCR00(?)";				
		    				    			    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setString(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMMSTR.FRID = rs.getInt(1);	
		        	PFRMMSTR.FRNAME = rs.getString(2).trim();
		        	PFRMMSTR.FREXTNL = rs.getString(3).trim();
		        	PFRMMSTR.FREXTHSH = rs.getString(4).trim();
		        }
		        
		        rs.close();
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
			
		    if(PFRMMSTR.FRID == 0){
		    	return "{\"ERROR\": \"Form does not exist\"}";
		    }
		    
		    if(PFRMMSTR.FREXTNL != "Y"){
		    	if(!authenticate.isSessionValid(conn, user, sessionID, sessionToken, sessionTimeout)){
		    		return "{\"ERROR\": \"Access Denied\"}";
		    	}
		    }		   
		    
			log.info("..retrieved record. retrieving detail records");
							
			stmt = null;
		    query = "CALL SFRMPRCR00(?)";				
		    				    			    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.setString(1, formid);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMPROP = new PFRMPROP();
		        	PFRMPROP.FPID = rs.getInt(1);	
		        	PFRMPROP.FPSEQ = rs.getInt(2);
		        	PFRMPROP.FPNAME = rs.getString(3).trim();
		        	PFRMPROP.FPTYPE = rs.getString(4).trim();
		        	PFRMPROP.FPREQ = rs.getString(5).trim();
		        	PFRMPROPs.add(PFRMPROP);
		        }		        		        
		        
		        rs.close();
		        stmt.close();
		        		     		        
		    } catch (SQLException e) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
					
			log.info("..retrieved " + PFRMPROPs.size() + " detail records. Retreiving field options.");
									
			try {
				for(int i=0; i<PFRMPROPs.size(); i++){
					stmt = null;
				    query = "CALL SFRMPOCR00(?,?)";		
				    
			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			        stmt.setInt(1,  PFRMPROPs.get(i).FPID);
			        stmt.setInt(2,  PFRMPROPs.get(i).FPSEQ);
			        stmt.execute();			        
			        
			        ResultSet rs = stmt.getResultSet();
			        while (rs.next()) {
			        	String reciever = rs.getString(1).trim();
			        	PFRMPROPs.get(i).FOVALUES.add(reciever);
			        }
			        
			        rs.close();
			        stmt.close();
			        
				}	        		        		     		        
		    } catch (SQLException e) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
				
			PFRMMSTR.PFRMPROP = PFRMPROPs;
			
			log.info("..retrieved field options.");
				 
			connectionHelper.closeConnection(conn);
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	  		json = ow.writeValueAsString(PFRMMSTR);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  		  		
  		
  		return json;
      }  
     
    @POST
 	@Path("/accessAllForms")
 	@Consumes(MediaType.APPLICATION_JSON)
 	@Produces(MediaType.APPLICATION_JSON)
 	public String accessAllForms(String masterString){
 		CONNECTOR CONNECTOR = new CONNECTOR(); 
 		Connection conn = null;
 		
 		ArrayList<PFRMMSTR> PFRMMSTRs = new ArrayList<PFRMMSTR>();
    	PFRMPROP PFRMPROP = new PFRMPROP();
    	ArrayList<PFRMPROP> PFRMPROPs = new ArrayList<PFRMPROP>();
 		
 		log.info("..mapping JSON string");
 		
 		JsonElement jelement = new JsonParser().parse(masterString);
 	    JsonObject  jobject = jelement.getAsJsonObject();
 	    String lib = jobject.get("lib").toString();
 	    lib = lib.substring(1, lib.length()-1);
 	    String user = jobject.get("user").toString();
 	    user = user.substring(1, user.length()-1);
    	
    	String json = "";
    	
  		try
		{	
  			CONNECTOR = connectionHelper.getConnection(lib);
  			conn = CONNECTOR.conn;
  			
			log.info("..calling stored procedure");
			
			CallableStatement stmt = null;
		    String query = "CALL SFRMMSCR01()";				
		    				    			    
		    try {
		        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        stmt.execute();			        
		        
		        ResultSet rs = stmt.getResultSet();
		        while (rs.next()) {
		        	PFRMMSTR PFRMMSTR = new PFRMMSTR();
		        	PFRMMSTR.FRID = rs.getInt(1);	
		        	PFRMMSTR.FRNAME = rs.getString(2).trim();
		        	PFRMMSTR.FREXTNL = rs.getString(3).trim();
		        	PFRMMSTR.FREXTHSH = rs.getString(4).trim();
		        	PFRMMSTR.FRPORT = rs.getString("FRPORT");
		        	PFRMMSTR.FRPORTID = rs.getString("FRPORTID");
		        	PFRMMSTR.FRPORTLBL = rs.getInt("FRPORTLBL");
		        	if(rs.getString("FRPASSIGN") != null){
		        		if(rs.getString("FRPASSIGN").equals("N")){
		        			PFRMMSTR.FRPASSIGN = false;
		        		} else {
		        			PFRMMSTR.FRPASSIGN = true;
		        		}
		        	} else {
		        		PFRMMSTR.FRPASSIGN = false;
		        	}
		        	PFRMMSTRs.add(PFRMMSTR);
		        }
		        
		        rs.close();
		        stmt.close();
		        
		    } catch (SQLException e ) {
		    	log.warn(e);
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }	   
		    
			log.info("..retrieved record. retrieving detail records");
							
			stmt = null;				
		    				    			    
		    for(PFRMMSTR masterRec: PFRMMSTRs){
		    	try {
		    		query = "CALL SFRMPRCR00(?)";
		    		stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        	String tmps = String.valueOf(masterRec.FRID);
		        	stmt.setString(1, tmps);
		        	stmt.execute();	
		        	PFRMPROPs = new ArrayList<PFRMPROP>();
		        
		        	ResultSet rs = stmt.getResultSet();
		        	while (rs.next()) {
		        		PFRMPROP = new PFRMPROP();
		        		PFRMPROP.FPID = rs.getInt(1);	
		        		PFRMPROP.FPSEQ = rs.getInt(2);
		        		PFRMPROP.FPNAME = rs.getString(3).trim();
		        		PFRMPROP.FPTYPE = rs.getString(4).trim();
		        		PFRMPROP.FPREQ = rs.getString(5).trim();
		        		PFRMPROPs.add(PFRMPROP);
		        	}
		        	masterRec.PFRMPROP = PFRMPROPs;
		        	rs.close();
		        	stmt.close();
		        		     		        
		        } catch (SQLException e) {
		        	log.warn(e);
		        } finally {
		        	if (stmt != null) { stmt.close(); }
		        }
					
		        log.info("..retrieved " + masterRec.PFRMPROP.size() + " detail records. Retreiving field options.");
									
		        try {
		        	for(PFRMPROP underRec:masterRec.PFRMPROP){
		        		stmt = null;
		        		query = "CALL SFRMPOCR00(?,?)";		
				    
		        		stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		        		stmt.setInt(1, underRec.FPID);
		        		stmt.setInt(2, underRec.FPSEQ);
		        		stmt.execute();			        
			        
		        		ResultSet rs = stmt.getResultSet();
		        		while (rs.next()) {
		        			String reciever = rs.getString(1).trim();
		        			underRec.FOVALUES.add(reciever);
		        		}
			        
		        		rs.close();
		        		stmt.close();
			        
		        	}	        		        		     		        
		        } catch (SQLException e) {
		        	log.warn(e);
		        } finally {
		        	if (stmt != null) { stmt.close(); }
		        }
		    }    
			
			log.info("..retrieved field options.");
				 
			connectionHelper.closeConnection(conn);
			
	  		json = new Gson().toJson(PFRMMSTRs);
			
		} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  		  		
  		
  		return json;
      }
     
     @GET
  	@Path("/getProcessTasks/{lib}")
  	@Produces(MediaType.APPLICATION_JSON)
  	public String getProcessTasks(@PathParam("lib")String lib){
  		CONNECTOR CONNECTOR = new CONNECTOR(); 
  		Connection conn = null;
  		
  		ProcessTypeRecord ProcessTypeRecord = new ProcessTypeRecord();
    	ArrayList<ProcessTypeRecord> ProcessTypeRecords = new ArrayList<ProcessTypeRecord>();
  		
  		String json = "";
  		
  		if(lib.equals("undefined")){
  			return json;
  		}
  		
  	    try
  	    	{	
//  	    		log.info("...datasource: " + lib);
  		        CONNECTOR = connectionHelper.getConnection(lib);
  	  			conn = CONNECTOR.conn;
  		        
  	  			log.info("..calling SP: SPMPTCR02");
  		        
  				CallableStatement stmt = null;
  			    String query = "CALL SPMPTCR02(?,?,?)";
  			    
  			    try {
  			        stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

  			        stmt.setString(1, "");
  			        stmt.setString(2, "N");
  			        stmt.setString(3, "B");
  			        stmt.execute();			        
  			        
  			        ResultSet rs = stmt.getResultSet();
  			        
  			      while (rs.next()) {
  		        	ProcessTypeRecord = new ProcessTypeRecord();
  		        	
  		        	ProcessTypeRecord.PORT = "P";
  		        	ProcessTypeRecord.TKTYPE = rs.getString("TKTYPE");	
  		        	ProcessTypeRecord.TKTMPNM = rs.getString("TKTMPNM");      	
  		        	
  		        	ProcessTypeRecords.add(ProcessTypeRecord);
  			      }  			        
  			        
  			        rs.close();
  			        stmt.close();
  			        
  			       log.info("....Task records added"); 
  			        
  			        json = new Gson().toJson(ProcessTypeRecords);
  			    } catch (SQLException e ) {
  			    	log.warn(e);
  			    } finally {
  			        if (stmt != null) { stmt.close(); }
  			    }
  			    connectionHelper.closeConnection(conn);
  	    	}
  	    catch(Exception ex)
  	    {
  	    	log.warn(ex);
			connectionHelper.closeConnection(conn);
			return "Error";
  	    }
  	    return(json);
  	}
}  
