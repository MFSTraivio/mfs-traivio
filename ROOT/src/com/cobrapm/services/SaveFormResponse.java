package com.cobrapm.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.cobrapm.authentication.CONNECTOR;
import com.cobrapm.authentication.ConnectionHelper;
import com.cobrapm.authentication.Encrypt;
import com.cobrapm.forms.PFRMRESP;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.IFSFileOutputStream;
   
@Path("SaveFormResponse")  
public class SaveFormResponse{ 
	private ConnectionHelper connectionHelper = new ConnectionHelper();
	//private AuthenticationHelper authenticate = new AuthenticationHelper();
	private static Logger log = Logger.getLogger(SaveFormResponse.class.getName());
	
	 @GET 
	 @Path("/saveResponse/{lib}/{formid}/{user}/{jobfunction}/{sessionID}/{sessionToken}/{sessionTimeout}/{directory}/{keyn1}/{keyn2}/{keyn3}/{responses}")  
     @Produces(MediaType.APPLICATION_JSON)
     public Response saveResponse(@PathParam("lib") String lib, @PathParam("formid") String formid, @PathParam("user") String user,
    		 @PathParam("jobfunction") String jobfunction, @PathParam("sessionID") BigDecimal sessionID,
    		 @PathParam("sessionToken") BigDecimal sessionToken, @PathParam("sessionTimeout") BigDecimal sessionTimeout, @PathParam("directory") String directory, 
    		 @PathParam("keyn1") int keyn1, @PathParam("keyn2") int keyn2, 
    		 @PathParam("keyn3") int keyn3, @PathParam("responses") String responses) throws IOException {  

    	CONNECTOR CONNECTOR = new CONNECTOR(); 
    	Connection conn = null;
 	
    	ArrayList<PFRMRESP> PFRMRESPs = new ArrayList<PFRMRESP>();
//    	Encrypt encrypt = new Encrypt();
    	
    	log.info("..mapping JSON string");
    	
    	ObjectMapper mapper = new ObjectMapper();
    	PFRMRESPs = mapper.readValue(responses, new TypeReference<ArrayList<PFRMRESP>>(){});
    	
//    	lib = encrypt.decryptLibrary(lib);
    	
    	int FERID = 0;
    	
	  	try{									
	  		CONNECTOR = connectionHelper.getConnection(lib);
	  		conn = CONNECTOR.conn;
	  			
			log.info("..adding master record");
				
			CallableStatement stmt = null;
			String query = "CALL SFRMRERU00(?,?,?,?,?)";				
			    				    			    
			try {
				stmt = conn.prepareCall(query);
			    stmt.registerOutParameter(2, Types.INTEGER);
			    stmt.registerOutParameter(5, Types.VARCHAR);
			    stmt.setInt(1, PFRMRESPs.get(0).FEID);
			    stmt.setInt(2, PFRMRESPs.get(0).FERID);
			    stmt.setString(3, jobfunction);
			    stmt.setString(4,  user);
			    stmt.executeUpdate();	
			        
//			        FERID = stmt.getInt(2);	 
//			        for(int i=0; i<PFRMRESPs.size(); i++){
			    FERID = stmt.getInt(2);
//			        }
			               			        
			    stmt.close();
			        
			} catch (SQLException e ) {
			    log.warn(e.getMessage());
			    return Response.status(500).entity(e.getMessage()).build(); 
			} finally {
			    if (stmt != null) { stmt.close(); }
			}
			
			log.info("...If We're tying to a Task, make the link.");
		    
		    if(keyn1 != 0 || keyn2 != 0){
		    	stmt = null;
			    query = "CALL SPRELRU00(?,?,?,?,?,?,?)";				
			    				    			    
			    try {
				        stmt = conn.prepareCall(query);
				        stmt.registerOutParameter(7, Types.VARCHAR);
				        stmt.setInt(1, keyn1);
				        stmt.setInt(2, keyn2);
				        stmt.setInt(3, keyn3);
				        stmt.setString(4, "F");
				        stmt.setInt(5, PFRMRESPs.get(0).FEID);
				        stmt.setInt(6, FERID);
				        stmt.executeUpdate();			        
				        
				        stmt.close();		     		        
			    } catch (SQLException e) {
			    	log.warn(e.getMessage());
			    	return Response.status(500).entity(e.getMessage()).build(); 
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
		    }
						
			log.info("..added master record. Adding children");
			for(PFRMRESP frmResponse: PFRMRESPs){					
				stmt = null;
			    query = "CALL SFRMRERU01(?,?,?,?,?)";				
			    				    			    
			    try {
//			    	for(int i=0; i<PFRMRESPs.size(); i++){
				        stmt = conn.prepareCall(query);
				        stmt.registerOutParameter(5, Types.VARCHAR);
				        stmt.setInt(1, frmResponse.FEID);
				        stmt.setInt(2, frmResponse.FERID);
				        stmt.setInt(3, frmResponse.FESEQ);
				        stmt.setString(4, frmResponse.FEDATA);				        
				        stmt.executeUpdate();			        
				        
				        stmt.close();
//			    	}		     		        
			    } catch (SQLException e) {
			    	log.warn(e.getMessage());
			    	return Response.status(500).entity(e.getMessage()).build(); 
			    } finally {
			        if (stmt != null) { stmt.close(); }
			    }
	  		
				log.info("..Added children. Uploading documents");
			    
				String fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
				fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));		        		
				fullpath = fullpath + File.separator + "uploads" + File.separator + directory;	
												
				File dir = new File(fullpath);
				File[] directoryListing = dir.listFiles();
				  if (directoryListing != null) {					  					
					  
					// Create an AS400 object.  This new directory will 
			        // be created on this system.
					Context initialContext = (Context)new InitialContext().lookup("java:comp/env");
				    String dbhost = (String)initialContext.lookup("dbhost");  					  
					String userid = (String)initialContext.lookup("dbuser");  
					String password = (String)initialContext.lookup("dbpassword");  
					  
			        AS400 sys = new AS400(dbhost, userid, password);
			        
			        // /$Library
			        String directorypath = lib;  
			        createDirectory(sys, directorypath);
			        
			        // /$Library/Forms
			        directorypath = lib + IFSFile.separator + "Forms";  
			        createDirectory(sys, directorypath);
			        
			        // /$Library/Forms/$Formid
			        directorypath = lib + IFSFile.separator + "Forms" + IFSFile.separator + formid;  
			        createDirectory(sys, directorypath);

			        // /$Library/Forms/$Formid/$thisresponsenumber
			        directorypath = lib + IFSFile.separator + "Forms" + IFSFile.separator + formid + IFSFile.separator + FERID;  
			        createDirectory(sys, directorypath);
			        
				    for (File child : directoryListing) {
				        // Create a file object that represents the directory.
				    	String filename = child.getName(); 
						FileInputStream sourceStream = new FileInputStream(fullpath + IFSFile.separator + filename);
				        IFSFileOutputStream destination = new IFSFileOutputStream(sys,  directorypath + IFSFile.separator + filename);
				        byte[] buffer = new byte[(int)child.length()];
				        sourceStream.read(buffer);
				        destination.write(buffer);
				   	 	destination.close(); 
				   	 	sourceStream.close();
				    }

				    FileUtils.deleteDirectory(dir);				    
				    sys.disconnectAllServices();
				    				    
				  }				 				  
				
			}
			connectionHelper.closeConnection(conn);								
				
	  	} catch(Exception ex) {
			log.warn(ex);
			connectionHelper.closeConnection(conn);
		}  		  		
  				
    	return Response.status(200).entity("success").build(); 
      } 
  		
	 
	  public void createDirectory(AS400 sys, String path){
		// Create a file object that represents the directory.
	        IFSFile aDirectory = new IFSFile(sys, path);

	        // Create the directory.
	        try {
				if (aDirectory.mkdir())
				   log.info("Create directory was successful");
				else
				{
				   // The create directory failed.
				   // If the object already exists, find out if it is a 
				   // directory or file, then display a message.
				   if (aDirectory.exists())
				   {
				      if (aDirectory.isDirectory())
				         log.info("Directory already exists");
				      else
				         log.info("Directory with this name already exists");
				   }
				   else
				      log.info("Create Directory failed");
				}
			} catch (IOException e) {			
				log.warn(e.getMessage());
			}
		  
	  }
	  
	  
	  @GET 
		 @Path("/saveResponseExt/{lib}/{formid}/{directory}/{name}/{email}/{responses}")  
	     @Produces(MediaType.APPLICATION_JSON)
	     public Response saveResponse(@PathParam("lib") String lib, @PathParam("formid") String formid, @PathParam("directory") String directory, 
	    		 @PathParam("name") String name, @PathParam("email") String email, 
	    		 @PathParam("responses") String responses) throws IOException {  

	    	CONNECTOR CONNECTOR = new CONNECTOR(); 
	    	Connection conn = null;
	 	
	    	ArrayList<PFRMRESP> PFRMRESPs = new ArrayList<PFRMRESP>();
//	    	Encrypt encrypt = new Encrypt();
	    	
	    	log.info("..mapping JSON string");
	    	
	    	ObjectMapper mapper = new ObjectMapper();
	    	PFRMRESPs = mapper.readValue(responses, new TypeReference<ArrayList<PFRMRESP>>(){});
	    	
	    	int FERID = 0;
	    	
	    	Encrypt encrypt = new Encrypt();
	    	lib = encrypt.decryptLibrary(lib);
	    	
	    	
		  	try{									
		  		CONNECTOR = connectionHelper.getConnection(lib);
		  		conn = CONNECTOR.conn;
		  			
				log.info("..adding master record");
					
				CallableStatement stmt = null;
				String query = "CALL SFRMRERU00(?,?,?,?,?)";				
				    				    			    
				try {
					stmt = conn.prepareCall(query);
				    stmt.registerOutParameter(2, Types.INTEGER);
				    stmt.registerOutParameter(5, Types.VARCHAR);
				    stmt.setInt(1, PFRMRESPs.get(0).FEID);
				    stmt.setInt(2, PFRMRESPs.get(0).FERID);
				    stmt.setString(3, name);
				    stmt.setString(4,  email);
				    stmt.executeUpdate();	

				    FERID = stmt.getInt(2);
				    
				    stmt.close();
				        
				} catch (SQLException e ) {
					log.warn(e.getMessage());
					return Response.status(500).entity(e.getMessage()).build(); 
				} finally {
				    if (stmt != null) { stmt.close(); }
				}
							
				log.info("..added master record. Adding children");
				for(PFRMRESP frmResponse: PFRMRESPs){					
				
					stmt = null;
				    query = "CALL SFRMRERU01(?,?,?,?,?)";				
				    				    			    
				    try {
//				    	for(int i=0; i<PFRMRESPs.size(); i++){
					        stmt = conn.prepareCall(query);
					        stmt.registerOutParameter(5, Types.VARCHAR);
					        stmt.setInt(1, frmResponse.FEID);
					        stmt.setInt(2, FERID);
					        stmt.setInt(3, frmResponse.FESEQ);
					        stmt.setString(4, frmResponse.FEDATA);				        
					        stmt.executeUpdate();			        
					        
					        stmt.close();
//				    	}		     		        
				    } catch (SQLException e) {
				    	log.warn(e.getMessage());
				    	return Response.status(500).entity(e.getMessage()).build(); 
				    } finally {
				        if (stmt != null) { stmt.close(); }
				    }
		  		
					log.info("..Added children. Uploading documents");
				    
					String fullpath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
					fullpath = fullpath.substring(0, fullpath.indexOf("WEB-INF"));		        		
					fullpath = fullpath + File.separator + "uploads" + File.separator + directory;	
													
					File dir = new File(fullpath);
					File[] directoryListing = dir.listFiles();
					  if (directoryListing != null) {					  					
						  
						// Create an AS400 object.  This new directory will 
				        // be created on this system.
						Context initialContext = (Context)new InitialContext().lookup("java:comp/env");
					    String dbhost = (String)initialContext.lookup("dbhost");  					  
						String userid = (String)initialContext.lookup("dbuser");  
						String password = (String)initialContext.lookup("dbpassword");  
						  
				        AS400 sys = new AS400(dbhost, userid, password);
				        
				        // /$Library
				        String directorypath = lib;  
				        createDirectory(sys, directorypath);
				        
				        // /$Library/Forms
				        directorypath = lib + IFSFile.separator + "Forms";  
				        createDirectory(sys, directorypath);
				        
				        // /$Library/Forms/$Formid
				        directorypath = lib + IFSFile.separator + "Forms" + IFSFile.separator + formid;  
				        createDirectory(sys, directorypath);

				        // /$Library/Forms/$Formid/$thisresponsenumber
				        directorypath = lib + IFSFile.separator + "Forms" + IFSFile.separator + formid + IFSFile.separator + FERID;  
				        createDirectory(sys, directorypath);
				        
					    for (File child : directoryListing) {
					        // Create a file object that represents the directory.
					    	String filename = child.getName(); 
							FileInputStream sourceStream = new FileInputStream(fullpath + IFSFile.separator + filename);
					        IFSFileOutputStream destination = new IFSFileOutputStream(sys,  directorypath + IFSFile.separator + filename);
					        byte[] buffer = new byte[(int)child.length()];
					        sourceStream.read(buffer);
					        destination.write(buffer);
					   	 	destination.close(); 
					   	 	sourceStream.close();
					    }

					    FileUtils.deleteDirectory(dir);				    
					    sys.disconnectAllServices();
					    				    
					  }				 				  
					
				}
				
				// Need to Kick Off Process?
				String TYPE = "";
				String NAME = "";
				String ASSIGN = "";
				String PORT = "";
				String LABEL = "";
				String shouldAssign = "";
				stmt = null;
				query = "CALL SFRMMSCR02(?,?)";				
				    				    			    
				try {
					stmt = conn.prepareCall(query,ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
				    stmt.setString(1, formid);
				    stmt.setInt(2, FERID);
				    stmt.execute();	

				    ResultSet rs = stmt.getResultSet();
			        while (rs.next()) {
			        	TYPE= rs.getString(1).trim();	
			        	NAME = rs.getString(2).trim();
			        	ASSIGN = rs.getString(3).trim();
			        	PORT = rs.getString(4).trim();
			        	LABEL = rs.getString(5).trim();
			        	shouldAssign = rs.getString(6).trim();
			        }
			        
			        rs.close();
				    
				    stmt.close();
				        
				} catch (SQLException e ) {
					log.warn(e.getMessage());
					return Response.status(500).entity(e.getMessage()).build(); 
				} finally {
				    if (stmt != null) { stmt.close(); }
				}
				
				if(shouldAssign.equals("Y")){
					ASSIGN = email;
				}
				
				if(TYPE != ""){
					Date currentDate = new Date();
					java.sql.Date sqlDate = new java.sql.Date(currentDate.getTime());
					Timestamp currentTimestamp = new Timestamp(currentDate.getTime());
					if(PORT.equals("P")){
						log.info("..Kicking Off Process");
						
						int keyn1 = 0;
						stmt = null;
					    query = "CALL SPPMPRU04(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";	
					    
						try {
							stmt = conn.prepareCall(query);
						    stmt.registerOutParameter(15, Types.VARCHAR);
						    stmt.registerOutParameter(16, Types.INTEGER);
						    stmt.setString(1, TYPE);
						    stmt.setString(2, LABEL);
						    stmt.setDate(3, sqlDate);
						    stmt.setString(4,  ASSIGN);
						    stmt.setString(5, "Y");
						    stmt.setString(6,  "");
						    stmt.setString(7, "");
						    stmt.setString(8, "");
						    stmt.setString(9, "");
						    stmt.setTimestamp(10, currentTimestamp);
						    stmt.setString(11,  email);
						    stmt.setString(12,  "SPPMPRU04");
						    stmt.setInt(13, 0);
						    stmt.setInt(14, 0);
						    stmt.executeUpdate();
						    
						    keyn1 = stmt.getInt(16);
						    
						    stmt.close();
						        
						} catch (SQLException e ) {
							log.warn(e.getMessage());
							return Response.status(500).entity(e.getMessage()).build(); 
						} finally {
						    if (stmt != null) { stmt.close(); }
						}
					    
						stmt = null;
						query = "CALL SPRELRU00(?,?,?,?,?,?,?)";				
						    				    			    
						try {
							stmt = conn.prepareCall(query);
							stmt.registerOutParameter(7, Types.VARCHAR);
							stmt.setInt(1, keyn1);
							stmt.setInt(2, 0);
							stmt.setInt(3, 0);
							stmt.setString(4, "F");
							stmt.setInt(5, PFRMRESPs.get(0).FEID);
							stmt.setInt(6, FERID);
							stmt.executeUpdate();			        
							        
							stmt.close();		     		        
						} catch (SQLException e) {
						    log.warn(e.getMessage());
						    return Response.status(500).entity(e.getMessage()).build(); 
						} finally {
						    if (stmt != null) { stmt.close(); }
						}
					} else {
						log.info("..Kicking Off Task");
						
						int keyn1 = 0;
						int keyn2 = 0;
						int keyn3 = 0;
						
						stmt = null;
					    query = "CALL SPPMTRU00(?,?,?,?,?,?,?,?,?,?,?)";	
					    
						try {
							stmt = conn.prepareCall(query);
						    stmt.registerOutParameter(8, Types.VARCHAR);
						    stmt.registerOutParameter(9, Types.INTEGER);
						    stmt.registerOutParameter(10, Types.INTEGER);
						    stmt.registerOutParameter(11, Types.INTEGER);
						    stmt.setString(1, TYPE);
						    stmt.setString(2, LABEL);
						    stmt.setDate(3, sqlDate);
						    stmt.setString(4,  ASSIGN);
						    stmt.setString(5, "Y");
						    stmt.setInt(6, 0);
						    stmt.setInt(7, 0);
						    stmt.executeUpdate();	
						    
						    keyn1 = stmt.getInt(9);
						    keyn2 = stmt.getInt(10);
						    keyn3 = stmt.getInt(11);
						    
						    stmt.close();
						        
						} catch (SQLException e ) {
							log.warn(e.getMessage());
							return Response.status(500).entity(e.getMessage()).build(); 
						} finally {
						    if (stmt != null) { stmt.close(); }
						}
						
						stmt = null;
						query = "CALL SPRELRU00(?,?,?,?,?,?,?)";				
						    				    			    
						try {
							stmt = conn.prepareCall(query);
							stmt.registerOutParameter(7, Types.VARCHAR);
							stmt.setInt(1, keyn1);
							stmt.setInt(2, keyn2);
							stmt.setInt(3, keyn3);
							stmt.setString(4, "F");
							stmt.setInt(5, PFRMRESPs.get(0).FEID);
							stmt.setInt(6, FERID);
							stmt.executeUpdate();			        
							        
							stmt.close();		     		        
						} catch (SQLException e) {
						    log.warn(e.getMessage());
						    return Response.status(500).entity(e.getMessage()).build(); 
						} finally {
						    if (stmt != null) { stmt.close(); }
						}
					}
				}
					
				connectionHelper.closeConnection(conn);									
			} catch(Exception ex) {
				log.warn(ex);
				connectionHelper.closeConnection(conn);
			}  		  		
	  				
	    	return Response.status(200).entity("success").build(); 
	      } 

}  
