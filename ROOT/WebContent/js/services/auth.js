angular.module('cobraApp').controller('AuthenticateController',['$scope', '$location', '$sessionStorage', '$state', '$rootScope','$cookies', 'Messenger','$http','$uibModal',
 function($scope, $location, $sessionStorage, $state, $rootScope, $cookies, Messenger, $http, $uibModal) {
	$scope.userid = '';
	$scope.password = '';
	$scope.errorLabel = '';
	$scope.isRemember = false;
	
	var modalInstance = null;
	
	$scope.submitForm = function() {
		var error;
		
		$.getJSON( "./rest/AuthorizationService/authenticateUser/" + $scope.userid + "/" + $scope.password + "/" + $scope.isRemember, function( data ) {	
			var sessionTrack;
			$.each( data, function( key, val ) {
			    switch(key){
			    	case "isError":
			    		error = val;
			    		break;
			    	case "returnMessage":
			    		if(error == true){
			    			$scope.errorLabel = val;
			    			$scope.$apply();
			    		}
			    		break;
			    	case "userRecord":
			    		if(error == false){
			    			var userid;
			    			var datasource;
			    			var datasourceConnectedSystem;
			    			var sessionID;
			    			var sessionToken;
			    			var sessionTimeout;
			    			var applicationVersion;
			    			var fullFileSystemPath;
			    			var jobfunctiondescription;
							
			    			$.each(val, function( propkey, propval ) {			    			
					    		switch(propkey){
					    			case "userid":
					    				userid = propval;
					    				break;
					    			case "datasource":
					    				datasource = propval;
					    				break;
					    			case "sessionID":
					    				sessionID = propval;
					    				break;
					    			case "sessionToken":
					    				sessionToken = propval;
					   					break;
					    			case "sessionTimeout":
					    				sessionTimeout = propval;
					    				break;
				    				case "jobfunctiondescription":
				    					jobfunctiondescription = propval;
					    				break;
				    				case "sessionTrack":
				    					sessionTrack = propval;
					    				break;
				    				case "taskRights":
				    					$rootScope.userData = propval;
					    		 }		    			 
			    			});
			    			$sessionStorage.$default({
				    			userid: userid,
				    			datasource: datasource,
				    			sessionID: sessionID,
				    			sessionToken: sessionToken,
				    			sessionTimeout: sessionTimeout,
				    			jobfunctiondescription: jobfunctiondescription,
				    			KEYN1: 0,
				    			KEYN2: 0,
				    			KEYN3: 0,
				    			formid: 0,
				    			formresp: 0
				    		});
			    			$sessionStorage.$save();
			    		}
			    	break;		
		    	}  	
		  	});
			if(error == false){
				if($scope.isRemember){
					var expire = new Date();
					expire.setDate(expire.getDate() + 31);
					$cookies.put('AUTH', sessionTrack, {'expires': expire});
				}
				Messenger.updateInfo($sessionStorage.datasource, $sessionStorage.userid);
				$rootScope.isLogin = false;
				$scope.loadNotifications();
			}
		});
	}
	
	$scope.loadNotifications = function(){
		$http({ 
			url:"./rest/NotificationService/retrieveNotifications",
			method: "POST",
			data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid},
			headers : {'Content-Type' : 'application/json'}}) 
		.then(function( response ) {
			$rootScope.notifications = response.data; 
			var count = 0;
			for(var i = 0; i < $rootScope.notifications.length; i++){
				if($rootScope.notifications[i].MsgRead != "Y"){
					count++;
				}
			}
			$rootScope.notification.unread = count;
			$state.go('dashboard');
		});
	};
	
	$scope.resetPassword = function () {
		  modalInstance = $uibModal.open({
			  animation: true,
		      templateUrl: './pages/modals/forgotPasswordModal.html',
		      controller: 'PasswordModalCtrl',
		      resolve: {}
		    });
		  
		    modalInstance.result.then(function(myjson){
		    	$http({ 
					url:"./rest/HtmlEmailSender/sendPasswordReset",
					method: "POST",
					data: {"lib":$sessionStorage.datasource, "email":myjson},
					headers : {'Content-Type' : 'application/json'}}) 
				.then(function( response ) {
					//Do Nothing
				});
		    });
		};
}]).controller('PasswordModalCtrl', function ($scope, $filter, $modalInstance) {
	
	$scope.email = {text:""};
	$scope.preSubmit = true;
	  	
	$scope.ok = function () {
		$scope.preSubmit = false;
		  
		setTimeout(function() {
			$modalInstance.close($scope.email.text);
		}, 3000);
	};

	$scope.cancel = function () {
	  $modalInstance.dismiss('cancel');
	};
});

angular.module('cobraApp').directive('ngEnter', function() {
	return function (scope, element, attrs) {
		element.bind("keydown keypress", function (event) {
			if(event.which === 13){
				scope.$apply( function() {
					scope.$eval(attrs.ngEnter);
				});
				event.preventDefault();
			}
		});
	};
}).controller('PasswordResetCtrl', ['$scope', '$filter', '$location', '$sessionStorage', '$window', 'blockUI','$state','$rootScope','$http',
    function ($scope, $filter, $location, $sessionStorage, $window, blockUI,$state, $rootScope, $http) {
		$scope.ID = "";
		$scope.email = "";
		$scope.pass = {
			pass: "",
			passCheck : ""
		};
		$scope.success = true;
		$scope.lineOne = "We're sorry, but your link seems to have expired."
		
		$scope.init = function () {
			$rootScope.isExtForm = true;
			$scope.ID = $location.search().t;
			$scope.email = $location.search().e;
			$http({ 
	    		url:"./rest/AuthorizationService/checkPasswordLink",
	    		method: "POST",
	    		data: {"token":$scope.ID, "email":$scope.email},
	    		headers : {'Content-Type' : 'application/json'}}) 
	    	.then(function( data ) {
	    		if(data.data != "Success"){
	    			$scope.success = false;
	    			//do something;
	    		}
	    	});
		};
  
		$scope.submitUserValidate = function() {
			if($scope.pass.pass != $scope.pass.passCheck){
				$scope.errorLabel = "Passwords do not match.";
			} else {
				$http({ 
					url:"./rest/AuthorizationService/updatePassword",
					method: "POST",
					data: {"email":$scope.email, "password":$scope.pass.pass},
	    			headers : {'Content-Type' : 'application/json'}}) 
	    		.then(function( data ) {
	    			$scope.isEditing = false;
	    			$rootScope.isLogin = true;
	    			$rootScope.isExtForm = false;
	    			$state.go('login');
	    		});
			}
		};
	
		$scope.init();

}]);