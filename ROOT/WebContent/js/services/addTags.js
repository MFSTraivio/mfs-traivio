angular.module('cobraApp').controller('AddTagsModalCtrl', function ($scope, $filter, $uibModalInstance, descriptions, sessionStorage, currentTags) {
	
	$scope.init = function() {
		var addNotesTags = document.getElementById('addNotesTags');
		$(addNotesTags).tagit({
			availableTags: descriptions
		});
	
		try {
			for(var i = currentTags.length; i >=0; i--){
				$(addNotesTags).tagit("createTag", currentTags[i]);
			};
		} catch (exc){
			if(currentTags != ""){
				$(addNotesTags).tagit("createTag", currentTags);
			}
		}
	};
	
	  $scope.ok = function () {
		  var tagsOut = $("#addNotesTags").tagit("assignedTags");
		  
		  var JSONResponse = angular.toJson(tagsOut);
		  
		  $uibModalInstance.close(JSONResponse);
	  };

	  $scope.cancel = function () {
		  $uibModalInstance.dismiss('cancel');
	  };

});