angular.module('cobraApp').controller('UserSetupCtrl',
    function ($scope, $filter, $location, $sessionStorage, $window, blockUI,$state, $rootScope, $http) {
		$scope.lib = "";
		$scope.group = "";
		$scope.sec = "";
		$scope.email = "";
		$scope.fname = "";
		$scope.lname = "";
		$scope.pass = "";
		
		$scope.init = function () {
			$rootScope.isExtForm = true;
			$scope.group = $location.search().g;
			$scope.lib = $location.search().l;
			$scope.sec = $location.search().s;
			$scope.email = $location.search().e;
		};
  
		$scope.submitUserValidate = function() {
			$http({ 
	    		url:"./rest/AdminService/addNewUser",
	    		method: "POST",
	    		data: {"lib":$scope.lib, "fname":$scope.fname, "email":$scope.email, 
	    				"lname":$scope.lname, "security":$scope.sec, "group":$scope.group, "pass":$scope.pass},
	    		headers : {'Content-Type' : 'application/json'}}) 
	    	.then(function( data ) {
	    		$scope.isEditing = false;
	    		$rootScope.isLogin = true;
				$state.go('login');
	    	});
		};
	
		$scope.init();

});