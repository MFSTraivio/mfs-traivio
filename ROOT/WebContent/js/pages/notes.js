
angular.module('cobraApp').controller('NotesCtrl',
function ($scope, $filter, $location, $sessionStorage, $window, blockUI, $uibModal, $http) {
    $scope.context = {
    		documentFolder: ""
    };
    
//    $scope.note = [];
    
    $scope.editNote = {};
    
    $scope.editCount = "";
    $scope.editText = "";
    $scope.editID = "";
    $scope.currentTags = [];
    
    $scope.notes = [];
    
    $scope.attach = {
    		NANOTEID: "",
			NAID: "",
			NAPATH: "",
			NAFILENAME: "",
			NAFILENAUN: ""
    };
    
    $scope.tag = {
    		PRNOTEID: "",
    		RECID: "",
    		PRDID: "",
    		DESC: ""
    };

    $scope.N5ID_cmt = 0;
    
    $scope.welcomeUser = "";
    $scope.descriptions = [];
    	
    $scope.PRELDESC = {
    		RECID: "",
    		PRDID: "",
    		PRDESC: ""
    };
    
    $scope.PRELDESCs = [];
    $scope.noteid = "";
    
    $scope.isAddNew = false;
    $scope.noteText = "";
    $scope.pinnedCheck = {
    	isPinned: false,
    	actualCheck: false
    };
    $scope.pinnedNote = {};
    
    $scope.commentText = "";
    $scope.editoring = false;
    
    var modalInstance = null;
    
    var PNOTE2RELs = [];

    $scope.init = function(){
    		
    	$scope.loadProcess();
    	
    }

    $scope.loadProcess = function() {
    	
    	document.title = "Notes";
    	
    	blockUI.delay = 100;
	  	blockUI.start();
	  	  
    	$scope.PRELDESCs = [];
    	$scope.descriptions = [];
    	var count = 0;
    	
    	$http({ 
			url:"./rest/NotesService/getAllTags",
			method: "POST",
			data: {"lib":$sessionStorage.datasource},
			headers : {'Content-Type' : 'application/json'}}) 
			.then(function( response ) {
				$scope.PRELDESCs = response.data;
				for(var i = 0; i < $scope.PRELDESCs.length; i++){
					$scope.descriptions.push($scope.PRELDESCs[i].PRDESC)
				}
    		$("#notesTags").tagit({
				availableTags: $scope.descriptions
			});
    	
    		if($sessionStorage.KEYN1 != 0 || $sessionStorage.KEYN2 != 0) {
    			$scope.notes = [];
    			$http({ 
    				url:"./rest/NotesService/getNotesByProcess",
    				method: "POST",
    				data: {"lib":$sessionStorage.datasource, "KEYN1":$sessionStorage.KEYN1, "KEYN2":$sessionStorage.KEYN2, "KEYN3":$sessionStorage.KEYN3},
    				headers : {'Content-Type' : 'application/json'}}) 
    				.then(function( response ) {
    					var tagCheck = [];
    					if($sessionStorage.KEYN1 != 0) {
    						$("#notesTags").tagit("createTag","Process: " + $sessionStorage.KEYN1);
    					}
    					if($sessionStorage.KEYN2 != 0) {
    						$("#notesTags").tagit("createTag","Task: " + $sessionStorage.KEYN2);
    					}
    					$scope.notes = response.data;
    					for(var k = 0; k < $scope.notes.length; k++){
    						for(var i = 0; i < $scope.notes[k].PNOTE2RELs.length; i++){
    							var found = false;
    							if(tagCheck.indexOf($scope.notes[k].PNOTE2RELs[i].DESC) > -1){
    								found = true;
    							}
    							if(!found){
    								$("#notesTags").tagit("createTag", $scope.notes[k].PNOTE2RELs[i].DESC);
    								tagCheck.push($scope.notes[k].PNOTE2RELs[i].DESC);
    							}
    						}
    					}
    				});
    			setTimeout(function(){
    				$("#notesTags").tagit({afterTagAdded: searchTags});
    				$("#notesTags").tagit({afterTagRemoved: searchTags});
    				blockUI.stop();
    			}, 1000);
    		} else {
    			$("#notesTags").tagit({afterTagAdded: searchTags});
    			$("#notesTags").tagit({afterTagRemoved: searchTags});
    		}
    		$("#fancyboxes").on("mouseenter", function(){
    			$("a.fancybox").fancybox();
    		});
    		if(!$scope.$$phase){
    			$scope.$apply(function() {
    				blockUI.stop();
    			});
    		}
    	});
    }

    $scope.addComment = function(id){
    	$scope.N5ID_cmt = id;
    }
    
    $scope.updateNotePre = function(id){
    	$scope.editID = id;
    	for(var i = 0; i < $scope.notes.length; i++){
    		if($scope.notes[i].N5ID == id){
    			$scope.editNote = $scope.notes[i];
    			$scope.editText = $scope.notes[i].N5TEXT;
    			$scope.editCount = $scope.notes[i].count;
    		}
    	}
    	$scope.editoring = true;
    }
    
    $scope.deleteNote = function(id){
    	$("#notesTags").tagit({afterTagAdded: null});
    	$("#notesTags").tagit({afterTagRemoved: null});
    	$http({ 
    		url:"./rest/NotesService/deleteNotes",
    		method: "POST",
    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "noteID":id},
    		headers : {'Content-Type' : 'application/json'}}) 
    		.then(function( data ) {
        		var a;
        		var b;
        		if($sessionStorage.KEYN1 != 0 || $sessionStorage.KEYN2 != 0) {
        			$("#notesTags").tagit("removeAll");
        			$("#addNotesTags").tagit("removeAll");
        			$scope.loadProcess();
        		} else {
        			searchTags(a,b);
        		}
        	});
    }

    function searchTags(event, ui){
    	var currentTags = $("#notesTags").tagit("assignedTags");
    	var DescsOut = [];
    	for(var i = 0; i < currentTags.length; i++){
    		for(var key in $scope.PRELDESCs){
    			if(currentTags[i] == $scope.PRELDESCs[key].PRDESC){
    				DescsOut.push($scope.PRELDESCs[key]);
    				break;
    			}
    		}
    	}
    	if($sessionStorage.KEYN1 != 0) {
    		var found = false;
    		for(var i = 0; i < currentTags.length; i++){
    			if(currentTags[i] == "Process: " + $sessionStorage.KEYN1){
    				found = true;
    			}
    		}
    		if(!found){
    			$sessionStorage.KEYN1 = 0;
    			$sessionStorage.$save();
    		}
		}
		if($sessionStorage.KEYN2 != 0) {
			var found = false;
    		for(var i = 0; i < currentTags.length; i++){
    			if(currentTags[i] == "Task: " + $sessionStorage.KEYN2){
    				found = true;
    			}
    		}
    		if(!found){
    			$sessionStorage.KEYN2 = 0;
    			$sessionStorage.KEYN3 = 0;
    			$sessionStorage.$save();
    		}
		}
    	if(DescsOut.length > 0) {
    		blockUI.start();
    		
    		$http({ 
    	    	url:"./rest/NotesService/getNotes",
    	    	method: "POST",
    	    	data: {"lib":$sessionStorage.datasource, "DescsOut":DescsOut},
    	    	headers : {'Content-Type' : 'application/json'}}) 
    	    .then(function( response ) {
    	    	$scope.notes = response.data;
    			for(var k = 1; k<=$scope.notes.length; k++){
    				$("#fancyboxes"+k).on("mouseenter", function(){
    					$("a.fancybox").fancybox();
    				});
    			}	
    			blockUI.stop();
    		});
    	} else {
    		$scope.notes = [];
    		$scope.pinnedNote = {};
    		if(!$scope.$$phase){
    			$scope.$apply(function() {
    				blockUI.stop();
    			});
    		}
    	}
    }

    $scope.submitForm = function(){

    	var currentTags = $("#notesTags").tagit("assignedTags");
    	var DescsOut = [];
    	for(var i = currentTags.length-1; i >= 0; i--){
    		if(currentTags[i] == "Task: " + $sessionStorage.KEYN2 || currentTags[i] == "Process: " + $sessionStorage.KEYN1){
    			currentTags.splice(i,1);
    		} else {
    			for(var key in $scope.PRELDESCs){
    				if($scope.PRELDESCs.hasOwnProperty(key)){
    					if(currentTags[i] == $scope.PRELDESCs[key].PRDESC){
    						DescsOut.push($scope.PRELDESCs[key]);
    						currentTags.splice(i,1);
    					}
    				}
    			}
    		}
    	}

    	if(currentTags.length != 0){
    		// To do - ahve to rebuild tags array before recheck
    		var currentTagsOut = JSON.stringify(currentTags);
    		$scope.PRELDESCs = [];
    		$.getJSON( "./rest/NotesService/updateTags/" + $sessionStorage.datasource + "/" + currentTagsOut, function( data ) {
    			$.each( data, function( key, val ) {
        			$scope.PRELDESC = {
        					RECID: val.RECID,
        					PRDID: val.PRDID,
        					PRDESC: val.PRDESC
        			};
        			$scope.descriptions.push(val.PRDESC);
        			$scope.PRELDESCs.push($scope.PRELDESC);
        			
        			$("#notesTags").tagit({
        				availableTags: $scope.descriptions
        			});
    			});
    			submitFormAfterCheck();
    		});
    	} else{
    		submitFormAfterCheck();
    	}
    }

    function submitFormAfterCheck(){
    	$("#notesTags").tagit({afterTagAdded: null});
    	$("#notesTags").tagit({afterTagRemoved: null});
    	var currentTags = $("#notesTags").tagit("assignedTags");
    	var DescsOut = [];
    	for(var i = currentTags.length-1; i >=0; i--){
    		for(var key in $scope.PRELDESCs){
    			if($scope.PRELDESCs.hasOwnProperty(key)){
    				if(currentTags[i] == $scope.PRELDESCs[key].PRDESC){
    					DescsOut.push($scope.PRELDESCs[key]);
    					currentTags.splice(i,1);
//    					break;
    				}
    			}
    		}
    	}
    	$scope.noteText = fixedEncodeURIComponent($scope.noteText);
    	var PinnedInfo = "";
    	if($scope.pinnedCheck.isPinned){
    		PinnedInfo = 'Y';
    	} else{
    		PinnedInfo = 'N';
    	}
    	
    	$http({ 
    		url:"./rest/NotesService/insertNotes",
    		method: "POST",
    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "noteText":$scope.noteText, "descsOut":DescsOut, "isPinned":PinnedInfo,
    				"KEYN1":$sessionStorage.KEYN1, "KEYN2":$sessionStorage.KEYN2, "KEYN3":$sessionStorage.KEYN3, "documentFolder":$scope.documentFolder},
    		headers : {'Content-Type' : 'application/json'}}) 
    	.then(function( data ) {
    		$scope.isAddNew = false;
    		$scope.noteText = "";
    		$scope.pinnedCheck.isPinned = false;
    		var a;
    		var b;
    		if($sessionStorage.KEYN1 != 0 || $sessionStorage.KEYN2 != 0) {
    			$("#notesTags").tagit("removeAll");
    			$("#addNotesTags").tagit("removeAll");
    			$scope.loadProcess();
    		} else {
    			searchTags(a,b);
    		}
    	});
    }

    $scope.updateNotePost = function(isPinned){
    	if(!isPinned){
    		for(var i = 0; i < $scope.notes.length; i++){
    			if($scope.notes[i].N5ID == $scope.editID){
    				$scope.notes[i].N5TEXT = $scope.editText;
    			}
    		}
    	} else {
    		$scope.editText = $scope.pinnedNote.N5TEXT;
    		$scope.editID = $scope.pinnedNote.N5ID;
    	}
    	$scope.editText = fixedEncodeURIComponent($scope.editText);
    	$http({ 
    		url:"./rest/NotesService/updateNotes",
    		method: "POST",
    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "noteText":$scope.editText, "noteID":$scope.editID, "mode":"U"},
    		headers : {'Content-Type' : 'application/json'}}) 
    	.then(function( data ) {
    		//do nothing
    		$scope.editoring = false;
    	});
    }
    $scope.submitComment = function(){

    	$.getJSON( "./rest/NotesService/insertComment/" + $sessionStorage.datasource + "/" + $sessionStorage.userid + "/" + $scope.commentText +
    					"/" + $sessionStorage.KEYN1 + "/" + $sessionStorage.KEYN2 + "/" + $sessionStorage.KEYN3 + "/" + $scope.N5ID_cmt, function( data ) {
    		$.each( data, function( key, val ) {
    			if(!val.isError){
    				$scope.N5ID_cmt = 0;
    				$scope.commentText = "";
    				var a;
					var b;
    				searchTags(a,b);
    			}
    		});
    	});	
    }

    $scope.addNew = function(){
    	
    	$scope.isAddNew = true;
    	$scope.uniqueToken = maketoken();
    	$scope.documentFolder = $sessionStorage.datasource + "_" + $scope.uniqueToken;
    }
    
    $scope.addTags = function () {
    	var currentTags = [];
    	for(var i = 0; i < $scope.editNote.PNOTE2RELs.length; i++){
    		for(var key in $scope.PRELDESCs){
    			if($scope.PRELDESCs.hasOwnProperty(key)){
    				if($scope.editNote.PNOTE2RELs[i].RECID == $scope.PRELDESCs[key].RECID
    					&& $scope.editNote.PNOTE2RELs[i].PRDID == $scope.PRELDESCs[key].PRDID){
    						currentTags.push($scope.PRELDESCs[key].PRDESC);
    				}
    			}
    		}
    	}

		  modalInstance = $uibModal.open({
			  animation: true,
		      templateUrl: './pages/modals/addTagsModal.html',
		      controller: 'AddTagsModalCtrl',
		      resolve: {
		    	descriptions: function (){
		    		return $scope.descriptions;
		    	},
		    	currentTags: function () {
		    		return currentTags;
		    	},
			    sessionStorage: function () {
		        	return $sessionStorage;
			    }
		      }
		    });
		  
		    modalInstance.result.then(function(JSONResponse){
		    	var currentTags = $.parseJSON(JSONResponse);
		    	var DescsOut = [];
		    	$scope.editNote.tags = [];
		    	for(var i = currentTags.length-1; i >= 0; i--){
		    		if(currentTags[i] == "Task: " + $sessionStorage.KEYN2 || currentTags[i] == "Process: " + $sessionStorage.KEYN1){
		    			currentTags.splice(i,1);
		    		} else {
		    			for(var key in $scope.PRELDESCs){
		    				if($scope.PRELDESCs.hasOwnProperty(key)){
		    					if(currentTags[i] == $scope.PRELDESCs[key].PRDESC){
		    						DescsOut.push($scope.PRELDESCs[key]);
		    						$scope.editNote.tags.push($scope.PRELDESCs[key]);
		    						currentTags.splice(i,1);
		    					}
		    				}
		    			}
		    		}
		    	}
		    	
		    	if(currentTags.length != 0){
		    		// To do - ahve to rebuild tags array before recheck
		    		var currentTagsOut = JSON.stringify(currentTags);
		    		$scope.PRELDESCs = [];
		    		$.getJSON( "./rest/NotesService/updateTags/" + $sessionStorage.datasource + "/" + currentTagsOut, function( data ) {
		    			$.each( data, function( key, val ) {
		        			$scope.PRELDESC = {
		        					RECID: val.RECID,
		        					PRDID: val.PRDID,
		        					PRDESC: val.PRDESC
		        			};
		        			$scope.descriptions.push(val.PRDESC);
		        			$scope.PRELDESCs.push($scope.PRELDESC);
		    			});
		    			for(var i = currentTags.length-1; i >= 0; i--){
	        				for(var key in $scope.PRELDESCs){
			    				if($scope.PRELDESCs.hasOwnProperty(key)){
			    					if(currentTags[i] == $scope.PRELDESCs[key].PRDESC){
			    						$scope.editNote.PNOTE2RELs.push($scope.PRELDESCs[key]);
			    					}
			    				}
	        				}
	        			}
		    			submitTagsAfterCheck();
		    		});
		    	} else{
		    		submitTagsAfterCheck();
		    	}
		    });
		};
	
	function submitTagsAfterCheck(){
		$("#notesTags").tagit({afterTagAdded: null});
		$("#notesTags").tagit({afterTagRemoved: null});
	   	$http({ 
	    		url:"./rest/NotesService/updatesTagsForNote",
	    		method: "POST",
	    		data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "noteID":$scope.editNote.N5ID, "tags":$scope.editNote.PNOTE2RELs},
	    		headers : {'Content-Type' : 'application/json'}}) 
	   	.then(function( data ) {
	   			$scope.editoring = false;
	    		$scope.noteText = "";
	    		$scope.pinnedCheck.isPinned = false;
	    		var a;
	    		var b;
	    		if($sessionStorage.KEYN1 != 0 || $sessionStorage.KEYN2 != 0) {
	    			$("#notesTags").tagit("removeAll");
	    			$("#addNotesTags").tagit("removeAll");
	    			$scope.loadProcess();
	    		} else {
	    			searchTags(a,b);
	    		}
	    		setTimeout(function(){
	    			$("#notesTags").tagit({afterTagAdded: searchTags});
					$("#notesTags").tagit({afterTagRemoved: searchTags});
	    		}, 1000);
	    });
	}

    function maketoken(){
    	var text = "";
    	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    	for( var i=0; i < 10; i++ )
    		text += possible.charAt(Math.floor(Math.random() * possible.length));
    		return text;
    }
    
    function fixedEncodeURIComponent (str) {  
    	return encodeURIComponent(str).replace(/[!'()*]/g, escape);  
    } 
    $scope.init();
    
}).directive('dropZone', function() {
    
    
    return function(scope, element, attrs) {
      
      element.dropzone({ 
        url: "./rest/FileUploadNotes/upload",
        headers : { "directory": scope.documentFolder},
	    autoProcessQueue : true,
	    addRemoveLinks : true,
        init: function() {
          this.on("removedfile", function(file) { 
	    	$.getJSON( "./rest/FileUploadNotes/remove/" + scope.documentFolder + "/" + file.name );
          });
        }
        
      });
     
    }     
      
    }).filter('unsafe', function($sce) {
        return function(val) {
        return $sce.trustAsHtml(val);
        };
    });
