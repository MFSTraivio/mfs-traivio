
angular.module('cobraApp').controller('DashCtrl',
function ($scope, $filter, $location, $sessionStorage, $window, blockUI, $uibModal, $http, $state, $rootScope) {
  $scope.context = {
			uniqueToken : "",	
			documentFolder : ""
  };
  
  $scope.widget = { 
		  SeqNum: "",
		  Title: "",
		  PorT: "",
		  Assigned: "",
		  Role: "",
		  ProcessType: "",
		  Cat: "",
		  Cat1: "",
		  Cat2: "",
		  Cat3: "",
		  Color: "",
		  Mode: "",
		  Count: "",
		  CountOverdue: "",
		  CountDueToday: "",
		  isDisabled: false,
		  status: 0,
		  Order: 0
  };
  
  $scope.editwidget = { 
		  SeqNum: "",
		  Title: "",
		  PorT: "",
		  Assigned: "",
		  Role: "",
		  ProcessType: "",
		  Cat: "",
		  Cat1: "",
		  Cat2: "",
		  Cat3: "",
		  Color: "",
		  Mode: "",
		  Count: "",
		  CountOverdue: "",
		  CountDueToday: "",
		  isDisabled: false,
		  status: 0,
		  Order: 0
  };
  
  $scope.processTask = {};
  
  $scope.PorT = {
		  type: ""
  };
  
  $scope.Types = [{type: "Task"},{type: "Process"}];
 
  $scope.displaycolumns = [];
  
  $scope.descriptions = [];
  
  $scope.dummy = {};
  
  $scope.PRELDESC = {
  		RECID: "",
  		PRDID: "",
  		PRDESC: ""
  };
  
  $scope.PRELDESCs = [];
  
  $scope.columns = [];
  	  
  $scope.rowCollection = [];
  $scope.widgets = [];
  
  $scope.myjobfunction = {
		  role: "",
		  name: ""
  };
  $scope.jobfunction = {
		  role: "",
		  name: ""
  };
  
  $scope.jobfunctionMasters = [];

  $scope.myprocesstype = {
		  type: "",
		  name: ""
  }
  
  $scope.processtype = {
		  type: "",
		  name: ""
  }
  
  $scope.processtypes = [];
  
  $scope.selectedRow = {noteCount: 0};
  
  $scope.mycolor = "#ff7373";
  $scope.mytitle = "";
  
  $scope.buttonstyle = "font-size:14px;display:initial;";  
  $scope.theadmargin = "text-align:center !important;";  
  $scope.tbodyscroll = "overflow-y:auto; overflow-x: auto; text-align:center !important; cursor: pointer;";
  
  $scope.widgetClicked = 0;
  
  $scope.bigwidgettitle = "";
  
  $scope.oppositeText = "";
  
  $scope.isCollapsed = false;
  $scope.isProcess = false;
  
  $scope.widgetsSize = 0;
  
  $scope.emailDisabled = false;
  $scope.redForm = "./images/forms-red.png";
  $scope.grayForm = "./images/forms-gray.png";
  $scope.subProcImg = "./images/367-code-tree@2x-16x16.png";
  
  var toggler;
  
  var modalInstance = null;
  
  $scope.init = function () {
	  $scope.isLoading = false;
//	  $scope.emailDisabled = true;
	  blockUI.delay = 100;
	  blockUI.start();
	  $scope.rowCollection = [];
//	  $scope.displaycolumns = [];
	  var elements = document.getElementsByClassName("row");
		 for(var i=0; i<elements.length; i++) {
			 if(elements[i].id == "hideable"){
				 elements[i].style.display = "none";
			 }
		 }
	  $scope.isCollapsed = false;
	  
	  $scope.widgets = [];
	  
	  modalInstance = null;
	  
	  var rownumber = 0;
	  
	  if($sessionStorage.datasource != undefined){
		  $.getJSON( "./rest/WidgetService/getWidget/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID, function( data ) {
			  $scope.widgets = data;
			  $scope.$apply(function() {
				  blockUI.stop();
				  $scope.loadJobFunctions();
				  $scope.widgetsSize = $scope.widgets.length;
			  });
			  for(var i = 0; i < $scope.widgets.length; i++){
				  var toggle = document.getElementById($scope.widgets[i].SeqNum)
				  $(toggle).toggle(false);
				  if($scope.widgets[i].Order == 0){
					  $scope.widgets[i].Order = i+1;
				  }
			  }
			  if($rootScope.widgetClicked != undefined){
				  $scope.widgetClicked = $rootScope.widgetClicked;
				  $scope.refresh();
			  }
		  });
	  }
  };
  
  $scope.loadJobFunctions = function() {
	  $.getJSON( "./rest/ProcessTaskService/getJobFunctions/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout, function( data ) {	  
			$.each( data, function( key, val ) {	  	 		 
		 		
				$scope.jobfunction = {
						role: val.KAJOBROLE,
						name: val.DisplayText
				};
				$scope.jobfunctionMasters.push($scope.jobfunction);
		 	  });
	  });
  };
  
  $scope.refresh = function () {
	  
	  blockUI.start();
	  $scope.query = "";
	  $scope.isCollapsed = true;
	  $scope.rowCollection = [];
	  $scope.displaycolumns = [];
//	  var rownumber = 0;
	  var JSONResponse;
	  
	  $scope.widgetOut = {};
	  
	  for(var i = 0; i < $scope.widgets.length; i++){
		  $scope.widgets[i].isDisabled=false;
		  var sizer = document.getElementById($scope.widgets[i].SeqNum).parentElement;
		  $(sizer).css({"width":""});
	  }

	  for(var i = 0; i < $scope.widgets.length; i++){
		  if($scope.widgets[i].SeqNum == $scope.widgetClicked){
			  $scope.bigwidgettitle = $scope.widgets[i].Title;
			  $scope.widgets[i].Color = $scope.widgets[i].Color.substring(1,$scope.widgets[i].Color.length);
//			  JSONResponse = encodeURIComponent(angular.toJson($scope.widgets[i]));
			  $scope.widgetOut = $scope.widgets[i];
			  $scope.widgets[i].Color = "#" + $scope.widgets[i].Color;
			  $scope.widgets[i].isDisabled=true;
			  $scope.displaycolumns = $scope.widgets[i].columns;
			  if($scope.widgets[i].PorT == "Task"){
				  $scope.oppositeText = "Open Process";
				  $scope.isProcess = false;
			  } else {
				  $scope.oppositeText = "Open Task";
				  $scope.isProcess = true;
			  }
			  var toggle = document.getElementById($scope.widgets[i].SeqNum).parentElement;
			  $(toggle).css({"width":"250px"});
		  } else{
			  var toggle = document.getElementById($scope.widgets[i].SeqNum);
			  $(toggle).toggle(false);
		  }
	  }
	  
	  $http({ 
		  url:"./rest/ProcessTaskService/getProcessTasks",
		  method: "POST",
		  data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "sessionID":$sessionStorage.sessionID, "widget":$scope.widgetOut},
		  headers : {'Content-Type' : 'application/json'}}) 
		  .then(function( data ) {
			  $scope.rowCollection = data.data;
			  for(var i = 0; i < $scope.rowCollection.length; i++){
				  $scope.rowCollection[i].StartDate = $filter('date')($scope.rowCollection[i].StartDate);
				  $scope.rowCollection[i].EndDate = $filter('date')($scope.rowCollection[i].EndDate);
			  }
			  
		 	 $scope.displayedCollection = [].concat($scope.rowCollection);	  		 
			 
		 	 blockUI.stop();
			 
			 var elements = document.getElementsByClassName("row");
			 for(var i=0; i<elements.length; i++) {
				 elements[i].style.display = "";
			 }
			 if($rootScope.rownumber != undefined){
				 for(var i = 0; i < $scope.rowCollection.length; i++){
					 if($scope.rowCollection[i].rownumber == $rootScope.rownumber){
						 $scope.rowCollection[i].isSelected = true;
						 $rootScope.rownumber = undefined;
						 $rootScope.widgetClicked = undefined;
					 }
				 }
			 }
			 if($rootScope.newProcNo != undefined){
				 if($rootScope.newProcNo != 0){
					 for(var i = 0; i < $scope.rowCollection.length; i++){
						 if($scope.rowCollection[i].TKNO == $rootScope.newProcNo){
							 $scope.rowCollection[i].isSelected = true;
							 $rootScope.ProcessStatus = $scope.rowCollection[i].Status;
							 $rootScope.ProcessDays = $scope.rowCollection[i].Days;
							 $sessionStorage.KEYN1 = $scope.rowCollection[i].TKNO;
							 $sessionStorage.KEYN2 = 0;
							 $sessionStorage.KEYN3 = 0;
							 $sessionStorage.$save();
							 $rootScope.isFormSubmit = true;
							 $rootScope.newProcNo = undefined;
							 $state.go('dashboard.process');
						 }
					 }
				 }
			 }
		  });	
  };
  
  $scope.getters = {
		  startDateSetter:function(row) {
			  return new Date(row.StartDate);
		  },
		  endDateSetter:function(row) {
			  return new Date(row.EndDate);
		  }
  }
  
  $scope.toggleSpaceIn = function (idIn,isDisabled) {
	  if(!isDisabled){
		  var toggle = document.getElementById(idIn);
		  toggler = setTimeout(function () {
			  $(toggle).toggle(true);  
		  }, 300);
	  }
  }
  
  $scope.toggleSpaceOut = function (idIn,isDisabled) {
	  if(!isDisabled){
		  var toggle = document.getElementById(idIn);
		  clearTimeout(toggler);
		  $(toggle).toggle(false);
	  }
  }
  
  $scope.showGrid = function (idIn) {
	  $scope.widgetClicked = idIn;
	  $scope.refresh();
  }
  
  $scope.startTask = function (clickEvent) {		
	  var result = document.getElementsByClassName("st-selected");
	  var wrappedResult = angular.element(result);
	  
	  var selectedRows = [];
	  for(var i = 0; i < wrappedResult.size(); i++){
		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
	  }
	  
	  if(selectedRows.length > 0){
		  var JSONResponse = encodeURIComponent(angular.toJson(selectedRows));
		  $.ajax({
			    type: "GET",
			    cache: false,		    
			    url: "./rest/ProcessTaskService/setStatus/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout + "/" + JSONResponse + "/S/" + false, 
			    complete: function(msg) {	
			    	if(msg.status == 200){ 
			    		$scope.refresh();
				    } else {
				    	console.log(msg.responseText);
				    }
			    }
			});		  
	  }		  
  };
  
  $scope.pauseTask = function (clickEvent) {		
	  var result = document.getElementsByClassName("st-selected");
	  var wrappedResult = angular.element(result);
	  
	  var selectedRows = [];
	  for(var i = 0; i < wrappedResult.size(); i++){
		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
	  }
	  
	  if(selectedRows.length > 0){
		  var JSONResponse = encodeURIComponent(angular.toJson(selectedRows));

		  $.ajax({
			    type: "GET",
			    cache: false,		    
			    url: "./rest/ProcessTaskService/setStatus/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout + "/" + JSONResponse + "/H/" + false, 
			    complete: function(msg) {	
			    	if(msg.status == 200){ 
			    		$scope.refresh();
				    } else {
				    	console.log(msg.responseText);
				    }
			    }
			});		  
	  }		  
  };
  
  $scope.completeTask = function (clickEvent) {		
	  var result = document.getElementsByClassName("st-selected");
	  var wrappedResult = angular.element(result);
	  
	  var selectedRows = [];
	  var SubProc = false;
	  for(var i = 0; i < wrappedResult.size(); i++){
		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
	  }
	  if(selectedRows.length > 0){
		  for(var i = 0; i < selectedRows.length; i ++){
			  if(selectedRows[i].SubProc != null){
				  SubProc = true;
			  }
		  }
		  var JSONResponse = encodeURIComponent(angular.toJson(selectedRows));
		  if(SubProc){
			  modalInstance = $uibModal.open({
				  animation: true,
			      templateUrl: './pages/modals/subProcModal.html',
			      controller: 'SubProcModalCtrl',
			      resolve: {}
			    });
			  modalInstance.result.then(function(answer){
				  $.ajax({
					    type: "GET",
					    cache: false,		    
					    url: "./rest/ProcessTaskService/setStatus/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout + "/" + JSONResponse + "/C/" + answer , 
					    complete: function(msg) {	
					    	if(msg.status == 200){ 
					    		$scope.refresh();
						    } else {
						    	console.log(msg.responseText);
						    }
					    }
					  });
			  });
		  } else {

			  $.ajax({
			    type: "GET",
			    cache: false,		    
			    url: "./rest/ProcessTaskService/setStatus/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout + "/" + JSONResponse + "/C/" + false, 
			    complete: function(msg) {	
			    	if(msg.status == 200){ 
			    		$scope.refresh();
				    } else {
				    	console.log(msg.responseText);
				    }
			    }
			  });
		  }
	  }		  
  };
  
  $scope.inactivateTask = function (clickEvent) {		
	  var result = document.getElementsByClassName("st-selected");
	  var wrappedResult = angular.element(result);
	  
	  var selectedRows = [];
	  for(var i = 0; i < wrappedResult.size(); i++){
		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
	  }
	  
	  if(selectedRows.length > 0){
		  var JSONResponse = encodeURIComponent(angular.toJson(selectedRows));
		  
		  $.ajax({
			    type: "GET",
			    cache: false,		    
			    url: "./rest/ProcessTaskService/setStatus/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + $sessionStorage.sessionToken + "/" + $sessionStorage.sessionTimeout + "/" + JSONResponse + "/I/" + false , 
			    complete: function(msg) {	
			    	if(msg.status == 200){ 
			    		$scope.refresh();
				    } else {
				    	console.log(msg.responseText);
				    }
			    }
			});		  
	  }		  
  };
  
  $scope.openForm = function(id) {
	  $sessionStorage.formid = id;
	  $sessionStorage.$save();
	  $rootScope.isFormsModal = true;
	  
	  $state.go('dashboard.forms');
  };
  
  $scope.openFinishedForm = function(id,resp) {
	  $sessionStorage.formid = id;
	  $sessionStorage.formresp = resp;
	  $sessionStorage.$save();
	  $rootScope.isFormsModal = true;
	  
	  $state.go('dashboard.forms');
  };
  
  $scope.openSpecificNotes = function (clickEvent) {		
	  var result = document.getElementsByClassName("st-selected");
	  var wrappedResult = angular.element(result);

	  var selectedRows = [];
	  for(var i = 0; i < wrappedResult.size(); i++){
		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
	  }
	  
	  $rootScope.widgetClicked = $scope.widgetClicked;
	  $scope.processTask = selectedRows[0];
	  $rootScope.rownumber = $scope.processTask.rownumber;
	  
	  $sessionStorage.KEYN1 = 0;
	  $sessionStorage.KEYN2 = 0;
	  $sessionStorage.KEYN3 = 0;
	  
	  if(selectedRows.length > 0) {
		  $sessionStorage.KEYN1 = $scope.processTask.TKNO;
		  $sessionStorage.KEYN2 = $scope.processTask.TSTSKNO;
		  $sessionStorage.KEYN3 = $scope.processTask.TSTSKSNO;
		  $sessionStorage.$save();
			
		  $location.path('/notes');   
	  }  
  };
  
  $scope.openSpecific = function (){
	  var result = document.getElementsByClassName("st-selected");
	  var wrappedResult = angular.element(result);

	  var selectedRows = [];
	  for(var i = 0; i < wrappedResult.size(); i++){
		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
	  }

	  $scope.processTask = selectedRows[0];
	  $rootScope.widgetClicked = $scope.widgetClicked;
	  $rootScope.ProcessStatus = $scope.processTask.Status;
	  $rootScope.ProcessDays = $scope.processTask.Days;
	  if(selectedRows.length > 0) {
		  if($scope.isProcess) {
			  $sessionStorage.KEYN1 = $scope.processTask.TKNO;
		  	  $sessionStorage.KEYN2 = 0;
		  	  $sessionStorage.KEYN3 = 0;
		  	  $sessionStorage.$save();
		  } else {
			  $sessionStorage.KEYN1 = $scope.processTask.TKNO;
			  $sessionStorage.KEYN2 = $scope.processTask.TSTSKNO;
			  $sessionStorage.KEYN3 = $scope.processTask.TSTSKSNO;
			  $sessionStorage.$save();
		  }
		  $rootScope.isFormSubmit = true;
		  $state.go('dashboard.process');
//		  $location.path('/MWS_PM/ProcessMain-en_US.html'); 
//		  var url = $location.path();
//		  $window.open(url); 	  
	  }  
  }
  
  $scope.openOppositeSpecific = function (){
	  var result = document.getElementsByClassName("st-selected");
	  var wrappedResult = angular.element(result);

	  var selectedRows = [];
	  for(var i = 0; i < wrappedResult.size(); i++){
		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
	  }

	  $scope.processTask = selectedRows[0];
	  
	  if(selectedRows.length > 0) {
		  if($scope.isProcess == false) {
			  $sessionStorage.KEYN1 = $scope.processTask.TKNO;
		  	  $sessionStorage.KEYN2 = 0;
		  	  $sessionStorage.KEYN3 = 0;
		  	  $sessionStorage.$save();
		  } else {
			  $sessionStorage.KEYN1 = $scope.processTask.TKNO;
			  $sessionStorage.KEYN2 = $scope.processTask.TSTSKNO;
			  $sessionStorage.KEYN3 = $scope.processTask.TSTSKSNO;
			  $sessionStorage.$save();
		  }
			
		  $location.path('/MWS_PM/ProcessMain-en_US.html'); 
		  var url = $location.path();
		  $window.open(url); 	  
	  }  
  }
  
  $scope.editWidgetDialog = function(idIn){
	  
	  if(idIn == 0){
		  $scope.displayedCollection = [];	  		 
		  $scope.displaycolumns = [];
		  $scope.editwidget = { 
				  SeqNum: "0",
				  Title: "",
				  PorT: "",
				  Assigned: "",
				  Role: "",
				  ProcessType: "",
				  Cat: "",
				  Cat1: "",
				  Cat2: "",
				  Cat3: "",
				  Color: "",
				  Mode: "",
				  Count: "",
				  CountOverdue: "",
				  CountDueToday: "",
				  isDisabled: false,
				  status: 0,
				  columns : []
		  };
		  
		  var elements = document.getElementsByClassName("row");
			 for(var i=0; i<elements.length; i++) {
				 if(elements[i].id != "stay"){
					 elements[i].style.display = "none";
				 }
			 }
			 for(var i = 0; i < $scope.widgets.length; i++){
				  $scope.widgets[i].isDisabled=false;
				  var sizer = document.getElementById($scope.widgets[i].SeqNum).parentElement;
				  $(sizer).css({"width":""});
				  var toggle = document.getElementById($scope.widgets[i].SeqNum);
				  $(toggle).toggle(false);
			  }
	  } else {
		  for(var i = 0; i < $scope.widgets.length; i++){
			  if($scope.widgets[i].SeqNum == idIn){
				  $scope.editwidget = $scope.widgets[i];
			  }
		  }  
	  }
	  
	  modalInstance = $uibModal.open({
		  animation: true,
	      templateUrl: './pages/modals/editWidgetModal.html',
	      controller: 'EditWidgetModalCtrl',
	      resolve: {
	        editwidget: function () {
	        	return $scope.editwidget;
		    },
		    sessionStorage: function () {
	        	return $sessionStorage;
		    }
	      }
	    });
	  modalInstance.result.then(function(myjson){
		  $scope.displaycolumns = [];
		  myjson = encodeURIComponent(myjson);
		  $.ajax({
			    type: "GET",
			    cache: false, 
			    url: "./rest/WidgetService/modifyWidget/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + myjson,
			    complete: function(msg) {
			    	if(msg.status == 200){ 
			    	 	try{
			    			$scope.init();
			    		} catch(err){
			    			console.log(err);
			    		}    
				    } else {
				    	console.log(msg.responseText);
				    }
			    }
		  });
//		  $http({ 
//			  url:"./rest/WidgetService/updateWidgetOrder",
//			  method: "POST",
//			  data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "widgets":$scope.widgets},
//			  headers : {'Content-Type' : 'application/json'}}) 
//			.then(function( data ) {
//			});
	  });
  };
  
  $scope.createNew = function () {
	  $rootScope.widgetClicked = $scope.widgetClicked;
	  modalInstance = $uibModal.open({
		  animation: true,
	      templateUrl: './pages/modals/createNewModal.html',
	      controller: 'CreateNewModalCtrl',
	      resolve: {
	        editwidget: function () {
	        	return $scope.editwidget;
		    },
		    sessionStorage: function () {
	        	return $sessionStorage;
		    }
	      }
	    });
	  
	    modalInstance.result.then(function(myjson){
	    	$scope.processString = angular.fromJson(myjson[0]);
	    	$scope.tagsstring = angular.fromJson(myjson[1]);
	    	$scope.udfs = angular.fromJson(myjson[2]);
	    	
	    	$http({ 
	  		  url:"./rest/ProcessTaskService/createProcessTask",
	  		  method: "POST",
	  		  data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "process":$scope.processString, "tags":$scope.tagsstring, "udfs":$scope.udfs},
	  		  headers : {'Content-Type' : 'application/json'}}) 
	  		  .then(function( response ) {
	  			try{
	  				$rootScope.newProcNo = response.data[0];
	    			$scope.init();
	    		} catch(err){
	    			console.log(err);
	    		}
	  		  });
//	    	$.ajax({
//			    type: "GET",
//			    cache: false, 
//			    url: "./rest/ProcessTaskService/createProcessTask/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + processtring + "/" + tagsstring,
//			    complete: function(msg) {
//			    	if(msg.status == 200){ 
//			    	 	try{
//			    			$scope.init();
//			    		} catch(err){
//			    			console.log(err);
//			    		}    
//				    } else {
//				    	console.log(msg.responseText);
//				    }
//			    }
//		  });
	    });
	};
	
	$scope.reassign = function () {
		  modalInstance = $uibModal.open({
			  animation: true,
		      templateUrl: './pages/modals/reassignModal.html',
		      controller: 'ReassignModalCtrl',
		      resolve: {
			    sessionStorage: function () {
		        	return $sessionStorage;
			    }
		      }
		    });
		  
		    modalInstance.result.then(function(myjson){
		    	var jobrole = encodeURIComponent(myjson);
		    	
		    	var result = document.getElementsByClassName("st-selected");
		  	  	var wrappedResult = angular.element(result);
		  	  
		  	  	var selectedRows = [];
		  	  	for(var i = 0; i < wrappedResult.size(); i++){
		  		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
		  	  	}
		  	  
		  	  	if(selectedRows.length > 0){
		  		  var JSONResponse = encodeURIComponent(angular.toJson(selectedRows));
		  		  
		  		  $.ajax({
				    type: "GET",
				    cache: false, 
				    url: "./rest/ProcessTaskService/reassignProcessTask/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + JSONResponse + "/" + jobrole,
				    complete: function(msg) {
				    	if(msg.status == 200){ 
				    	 	try{
				    			$scope.init();
				    		} catch(err){
				    			console.log(err);
				    		}    
					    } else {
					    	console.log(msg.responseText);
					    }
				    }
		  		  });
		  	  	}
		    });
		};
  
$scope.deleteWidget = function(idIn){
	  
	for(var i = 0; i < $scope.widgets.length; i++){
		  if($scope.widgets[i].SeqNum == idIn){
			  $scope.editwidget = $scope.widgets[i];
		  }
	  }
	  $scope.editwidget.Assigned =  $scope.myjobfunction.role;
	  $scope.editwidget.ProcessType = $scope.myprocesstype.type;
	  if($scope.editwidget.PorT == "Task"){
		  $scope.editwidget.PorT = "T";
	  } else{
		  $scope.editwidget.PorT = "P";
	  }
	  $scope.editwidget.Color = $scope.mycolor.substring(1,$scope.mycolor.length);

	  $scope.editwidget.Mode = "D";
	  
	  $scope.editwidget.columns = [];
	  var JSONResponse = angular.toJson($scope.editwidget);
	  var myjson = encodeURIComponent(JSONResponse);
	  $.ajax({
		    type: "GET",
		    cache: false, 
		    url: "./rest/WidgetService/modifyWidget/" + $sessionStorage.datasource +  "/" + $sessionStorage.userid + "/" + $sessionStorage.sessionID + "/" + myjson,
		    complete: function(msg) {
		    	if(msg.status == 200){ 
		    	 	try{
		    			$scope.init();
		    		} catch(err){
		    			console.log(err);
		    		}    
			    } else {
			    	console.log(msg.responseText);
			    }
		    }
	  });
  };
  
  $scope.rowSelected = function(){
	  var selectedRows = [];
	  
	  for(var i = 0; i < $scope.displayedCollection.length; i++){
		  if($scope.displayedCollection[i].isSelected){
			  selectedRows.push($scope.displayedCollection[i]);
		  }
	  }
	  if(selectedRows.length == 0 || selectedRows.length == 1){
		  $scope.emailDisabled = false;
	  } else {
		  $scope.emailDisabled = true;
	  }
	  if(selectedRows.length == 1){
		  $scope.selectedRow.noteCount = selectedRows[0].NoteCount;
	  } else {
		  $scope.selectedRow.noteCount = 0;
	  }
  }
  
  $scope.sendEmail = function() {
	  var result = document.getElementsByClassName("st-selected");
	  var wrappedResult = angular.element(result);

	  var selectedRows = [];
	  for(var i = 0; i < wrappedResult.size(); i++){
		  selectedRows.push($scope.rowCollection[wrappedResult[i].getAttribute("rownumber")]);
	  }
	  $scope.processTask = selectedRows[0];
	  
	  modalInstance = $uibModal.open({
		  animation: true,
	      templateUrl: './pages/modals/checkEmailModal.html',
	      controller: 'CheckEmailModalCtrl',
	      resolve: {
	    	  processTask: function () {
	        	return $scope.processTask;
		    }
	      }
	    });
	  
	    modalInstance.result.then(function(){
	    	var toAdd = "";
	    	for(var i = 0; i < $scope.jobfunctionMasters.length; i++){
	    		if($scope.jobfunctionMasters[i].name.trim() == $scope.processTask.Assigned.trim()){
	    			toAdd = $scope.jobfunctionMasters[i].role;
	    		}
			 };

			 var toNameArr = $scope.processTask.Assigned.split(" ");
			 var toName = toNameArr[0];
			 var fromNameArr = $sessionStorage.jobfunctiondescription.split(" ");
			 var fromName = fromNameArr[0];
			 $.ajax({
				 type: "GET",
				 cache: false, 
				 url: "./rest/HtmlEmailSender/sendHtmlEmail/"+toAdd+"/"+fromName+"/"+toName+"/"+$scope.processTask.TaskName,
				 complete: function(msg) {
					 if(msg.status == 200){ 
						 try{
							 $scope.init();
						 } catch(err){
							 console.log(err);
						 }    
					 } else {
						 console.log(msg.responseText);
					 }
				 }
			 });
	    });
  };
  
  $scope.$watchCollection('widgets', function(){
	  var found = false;
	  for(var i = 0; i < $scope.widgets.length; i ++){
		  if($scope.widgets[i].Order != 0){
			  if($scope.widgets[i].Order != i+1){
				  found = true;
			  } 
		  } else {
			  found = true;
		  }
	  }
	  
	  if(found && $scope.widgets.length >= $scope.widgetsSize){
		  for(var i = 0; i < $scope.widgets.length; i ++){
			  $scope.widgets[i].Order = i+1;
		  }
		  $http({ 
			  url:"./rest/WidgetService/updateWidgetOrder",
			  method: "POST",
			  data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "widgets":$scope.widgets},
			  headers : {'Content-Type' : 'application/json'}}) 
			.then(function( data ) {
			});
 		}
  });
  
  $scope.$watchCollection('displaycolumns', function(){
	  var found = false;
	  for(var i = 0; i < $scope.displaycolumns.length; i ++){
		  if($scope.displaycolumns[i].DSORDER != i+1){
			  found = true;
		  } 
	  }
	  
	  if(found){
		  for(var i = 0; i < $scope.displaycolumns.length; i ++){
			  $scope.displaycolumns[i].DSORDER = i+1;
		  }
		  $http({ 
			  url:"./rest/WidgetService/updateWidgetColumnOrder",
			  method: "POST",
			  data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "columns":$scope.displaycolumns},
			  headers : {'Content-Type' : 'application/json'}}) 
			.then(function( data ) {
			});
 		}
  });
  
  $scope.$watchCollection('columns', function(){
	  var found = false;
	  for(var i = 0; i < $scope.widgets.length; i ++){
		  if($scope.widgets[i].Order != 0){
			  if($scope.widgets[i].Order != i+1){
				  found = true;
			  } 
		  } else {
			  found = true;
		  }
	  }
	  
	  if(found && $scope.widgets.length >= $scope.widgetsSize){
		  for(var i = 0; i < $scope.widgets.length; i ++){
			  $scope.widgets[i].Order = i+1;
		  }
		  $http({ 
			  url:"./rest/WidgetService/updateWidgetOrder",
			  method: "POST",
			  data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid, "widgets":$scope.widgets},
			  headers : {'Content-Type' : 'application/json'}}) 
			.then(function( data ) {
			});
 		}
  });
  
  $scope.init();
	  
  $scope.checkCollapse = function(){
	var image = document.getElementById("collapseArrow");
	if($scope.isCollapsed){
		$(image).css({"background":"url(./images/arrow-down.png)"});
	} else {
		$(image).css({"background":"url(./images/arrow-up.png)"});
	}
  };

}).directive('stRatio',function(){
    return {
        link:function(scope, element, attr){
          var ratio=+(attr.stRatio);
          
          element.css('width',ratio+'%');
          
        }
      };
}).directive('stopEvent', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if(attr && attr.stopEvent)
                element.bind(attr.stopEvent, function (e) {
                    e.stopPropagation();
                });
        }
    };
  }).directive('stPaginationScroll', ['$timeout', function (timeout) {
      return{
          require: 'stTable',
          link: function (scope, element, attr, ctrl) {
              var itemByPage = 20;
              var pagination = ctrl.tableState().pagination;
              var lengthThreshold = 50;
              var timeThreshold = 400;
              var handler = function () {
                  //call next page
                  ctrl.slice(pagination.start + itemByPage, itemByPage);
              };
              var promise = null;
              var lastRemaining = 9999;
              var container = angular.element(element.parent());

              container.bind('scroll', function () {
                  var remaining = container[0].scrollHeight - (container[0].clientHeight + container[0].scrollTop);

                  //if we have reached the threshold and we scroll down
                  if (remaining < lengthThreshold && (remaining - lastRemaining) < 0) {

                      //if there is already a timer running which has no expired yet we have to cancel it and restart the timer
                      if (promise !== null) {
                          timeout.cancel(promise);
                      }
                      promise = timeout(function () {
                          handler();

                          //scroll a bit up
                          container[0].scrollTop -= 500;

                          promise = null;
                      }, timeThreshold);
                  }
                  lastRemaining = remaining;
              });
          }

      };      
  }]).directive('datepickerPopup', function (){
	    return {
	        restrict: 'EAC',
	        require: 'ngModel',
	        link: function(scope, element, attr, controller) {
	      //remove the default formatter from the input directive to prevent conflict
	      controller.$formatters.shift();
	  }
	}
}).directive('dateValidator', function() {
    return {
        require: 'ngModel',
        link: function (scope, elem, attr, ngModel) {
            function validate(value) {
                if (value !== undefined && value != null) {
                    ngModel.$setValidity('badDate', true);
                    if (value instanceof Date) {
                        var d = Date.parse(value);
                        // it is a date
                        if (isNaN(d)) {
                            ngModel.$setValidity('badDate', false);
                        }
                    } else {
                        var myPattern = new RegExp(/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/);
                        if (value !='' && !myPattern.test(value)) {
                            ngModel.$setValidity('badDate', false);
                        }
                    }
                }
            }

            scope.$watch(function () {
                return ngModel.$viewValue;
            }, validate);
        }
    };
}).directive('searchWatchModel',function(){
	  return {
		    require:'^stTable',
		    scope:{
		      searchWatchModel:'='
		    },
		    link:function(scope, ele, attr, ctrl){
		      var table=ctrl;
		      
		      scope.$watch('searchWatchModel',function(val){
		        ctrl.search(val);
		      });
		      
		    }
		  };
}).directive('resize', function ($window) {
	    return function (scope, element, attr) {
	    	
	        var w = angular.element($window);
	        scope.$watch(function () {
	            return {
	                'h': window.innerHeight, 
	                'w': window.innerWidth
	            };
	        }, function (newValue, oldValue) {
//	            console.log(newValue.w, oldValue.w);
	            scope.windowHeight = newValue.h;
	            scope.windowWidth = newValue.w;
	            
	            scope.columns[0].width = "0";
	            scope.columns[1].width = "0";
	            scope.columns[2].width = "0";
	            
	            scope.columns[0].colstyle = "display:none;";
	            scope.columns[1].colstyle = "display:none;";
	            scope.columns[2].colstyle = "display:none;";

	            scope.resizeWithOffset = function (offsetH) {
	                scope.$eval(attr.notifier);
	                scope.columns[3].width = "10";
                	scope.columns[4].width = "25";
                	scope.columns[5].width = "25";
                	scope.columns[6].width = "8";
                	scope.columns[7].width = "7";
                	scope.columns[8].width = "10";
                	scope.columns[9].width = "10";
                	scope.columns[10].width = "5";
                	scope.columns[3].colstyle = "text-align:center !important; color: black;";
                	scope.columns[4].colstyle = "display:flex; color: black;";
                	scope.columns[5].colstyle = "text-align:center !important; color: black;";
                	scope.columns[6].colstyle = "text-align:center !important; color: black;";
                	scope.columns[7].colstyle = "text-align:center !important; color: black;";
                	scope.columns[8].colstyle = "text-align:center !important; color: black;";
                	scope.columns[9].colstyle = "text-align:center !important; color: black;";
                	scope.columns[10].colstyle = "text-align:center !important; color: black;";                	                	
	                scope.displaycolumns = scope.columns;
	                
	                scope.buttonstyle = "font-size:14px;display:initial;";
	                
	                scope.theadmargin = "margin-right:17px;";  
	                scope.tbodyscroll = "overflow-y:scroll; cursor: pointer;";
	                
	                if(newValue.w <= 1210){
	                	scope.columns[4].width = "0";
	                	
	                	scope.columns[3].width = "10";	                		                 	
	                 	scope.columns[5].width = "35";
	                 	scope.columns[6].width = "15";
	                 	scope.columns[7].width = "15";
	                 	scope.columns[8].width = "0";
	                 	scope.columns[9].width = "25";
	                 	scope.columns[10].width = "0";
	                 	
	                 	scope.columns[4].colstyle = "display:none;";
	                 	scope.columns[8].colstyle = "display:none;";
	                 	scope.columns[10].colstyle = "display:none;";
	                	scope.displaycolumns = [scope.columns[3],scope.columns[5],scope.columns[6],scope.columns[7],scope.columns[9]];
	                	
	                	scope.buttonstyle = "font-size:10px;display:initial;";
	                }
	                if(newValue.w <= 500){
	                	scope.columns[4].width = "0";
	                 	scope.columns[6].width = "0";
	                 	scope.columns[7].width = "0";
	                 	scope.columns[8].width = "0";
	                 	scope.columns[10].width = "0";
	                 	
	                 	scope.columns[3].width = "20";
	                 	scope.columns[5].width = "50";
	                	scope.columns[9].width = "30";
	                 	
	                	scope.columns[4].colstyle = "display:none;";
	                	scope.columns[6].colstyle = "display:none;";
	                	scope.columns[7].colstyle = "display:none;";
	                	scope.columns[8].colstyle = "display:none;";
	                	scope.columns[10].colstyle = "display:none;";
	                	scope.displaycolumns = [scope.columns[3],scope.columns[5],scope.columns[9]];
	                	
	                	scope.buttonstyle = "font-size:10px;display:none;";
	                	
	                	scope.theadmargin = "margin-right:0px;";  
		                scope.tbodyscroll = "overflow-y:hidden; cursor: pointer;";
	                	
	                	return { 	                	
		                    'height': 'auto'               
		                };
	                }
	                return { 	                	
	                    'height': (newValue.h - offsetH) + 'px'                    
	                };
	            };

	        }, true);

	        w.bind('resize', function () {
	            scope.$apply();
	        });
	    }
	});