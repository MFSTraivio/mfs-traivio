angular.module('rcForm', [])
.directive(rcSubmitDirective);

angular.module('cobraApp', ['ngRoute', 'smart-table', 'ngStorage', 'blockUI', 'colorpicker.module', 'ngDragDrop', 'ui.bootstrap', 'ngAnimate', 
                            'monospaced.elastic', 'rcForm', 'xeditable', 'ui.router', 'ngResource','ngCookies', 'textAngular','pusher-angular',
                            'selectionModel','lrDragNDrop', 'ngMessages', 'ngWebSocket', 'angular-notification-icons', 'mgo-angular-wizard']);

angular.module('cobraApp').config(function($stateProvider, $urlRouterProvider){

	$urlRouterProvider.otherwise("/login");
	
	$stateProvider
	// route for the Login page
	.state('login', {
		url: "/login",
		templateUrl : 'pages/login.html',
		controller  : 'AuthenticateController'
	})

	// route for the main page
	.state('dashboard', {
		url: '/dashboard',
		templateUrl : 'pages/dashboard.html',
		controller  : 'DashCtrl'
	})
	
	// create a nested state
	.state('dashboard.forms', {
		views: {
			"modal" :{templateUrl: 'pages/modals/formsModal.html', controller : 'FormCtrl'}
		},
		onEnter: ["$state", function($state) {
		      $(document).on("keyup", function(e) {
		        if(e.keyCode == 27) {
		          $(document).off("keyup");
		          $state.go("dashboard");
		        }
		      });

//		      $(document).on("click", ".Modal-backdrop", function() {
//		        $state.go("dashboard");
//		      });

		      $(document).on("click", ".Modal-holder, .Modal-holder *", function(e) {
		        e.stopPropagation();
		      });
		    }]
	})

	// route for the contact page
	.state('notes', {
		url: '/notes',
		templateUrl : 'pages/notesMain.html',
		controller  : 'NotesCtrl'
	})
	
	// route for the contact page
	.state('forms', {
		url: '/forms',
		templateUrl : 'pages/forms.html',
		controller  : 'FormCtrl'
	})
	
	.state('forms.formsbuilder', {
		views: {
			"formsViewer" :{templateUrl: 'pages/formsbuilder.html'}
		}
	})
	
	.state('forms.formseditor', {
		views: {
			"formsViewer" :{templateUrl: 'pages/formseditor.html'}
		}
	})
	
	.state('forms.formmain', {
		views: {
			"formsViewer" :{templateUrl: 'pages/formsMain.html'}
		}
	})
	
	.state('eforms', {
		url: '/eforms',
		templateUrl : 'pages/eformsMain.html',
		controller  : 'eFormCtrl'
	})
	
	// route for the contact page
	.state('formlayout', {
		url: '/formslayout',
		templateUrl : 'pages/formslayout/formslayout.html',
		controller  : 'FormLayoutCtrl'
	})
	
	// route for the Admin page
	.state('account', {
		url: '/account',
		templateUrl : 'pages/accountMain.html',
		controller  : 'AccountCtrl'
	})
	
	.state('account.password', {
		views: {
			"accountViewer" :{templateUrl: 'pages/account/password.html'}
		}
	})
	
	.state('account.profile', {
		views: {
			"accountViewer" :{templateUrl: 'pages/account/profile.html'}
		}
	})
	
	.state('account.notifications', {
		views: {
			"accountViewer" :{templateUrl: 'pages/account/notifications.html'}
		}
	})
	
	.state('administration', {
		url: '/administration',
		templateUrl : 'pages/adminMain.html',
		controller  : 'AdminCtrl'
	})
	
	.state('userSetup', {
		url: '/userSetup',
		templateUrl : 'pages/userSetupMain.html',
		controller  : 'UserSetupCtrl'
	})
	
	.state('passwordReset', {
		url: '/passwordReset',
		templateUrl : 'pages/passwordResetMain.html',
		controller  : 'PasswordResetCtrl'
	})
	
	.state('administration.newUser', {
		views: {
			"adminViewer" :{templateUrl: 'pages/admin/addUser.html'}
		}
	})
	
	.state('administration.security', {
		views: {
			"adminViewer" :{templateUrl: 'pages/admin/securityAdmin.html'}
		}
	})
	
	.state('administration.groups', {
		views: {
			"adminViewer" :{templateUrl: 'pages/admin/groupAdmin.html'}
		}
	})
	
	.state('administration.required', {
		views: {
			"adminViewer" :{templateUrl: 'pages/admin/requiredAdmin.html'}
		}
	})
	
	.state('dashboard.process', {
		views: {
			"modal" :{templateUrl: 'pages/processMain.html', controller : 'ProcessCtrl'}
		},
		onEnter: ["$state", function($state) {
		      $(document).on("keyup", function(e) {
		        if(e.keyCode == 27) {
		          $(document).off("keyup");
		          $state.go("dashboard");
		        }
		      });

		      $(document).on("click", ".Modal-holder, .Modal-holder *", function(e) {
		        e.stopPropagation();
		      });
		    }]
	});

});

angular.module('cobraApp').controller('routeController',
	function($scope, $rootScope, $window, $location, $sessionStorage, $cookies, $state, $http, $pusher, Messenger){
	$rootScope.notification = {unread: 0};
	if($location.path() == '/login'){
		$scope.isRemembered = "";
		$scope.isRemembered = $cookies.get('AUTH');
		if($scope.isRemembered != undefined){
			$http({ 
				url:"./rest/AuthorizationService/retrieveLogin",
				method: "POST",
				data: {"sessionToken":$scope.isRemembered},
				headers : {'Content-Type' : 'application/json'}}) 
			.then(function( response ) {
				var userid;
				var datasource;
				var sessionID;
				var sessionToken;
				var sessionTimeout;
				var applicationVersion;
				var fullFileSystemPath;
				var jobfunctiondescription;
				$.each( response.data, function( key, val ) {
					switch(key){
    					case "userid":
    						userid = val;
    						break;
    					case "datasource":
    						datasource = val;
    						break;
    					case "sessionID":
    						sessionID = val;
    						break;
    					case "sessionToken":
    						sessionToken = val;
    						break;
    					case "sessionTimeout":
    						sessionTimeout = val;
    						break;
    					case "jobfunctiondescription":
    						jobfunctiondescription = val;
    						break;
    					case "taskRights":
    						$rootScope.userData = val;
//	    					$.each(val, function( k, v ) {
//	    						$scope.UserRights = {
//	    							Module: v.Module,
//	    							Value: v.RightValue
//	    						};
//	    						userData.taskRights.push($scope.UserRights);
//	    					});
    						break;
					}
				});
				$sessionStorage.$default({
					userid: userid,
					datasource: datasource,
					sessionID: sessionID,
					sessionToken: sessionToken,
					sessionTimeout: sessionTimeout,
					jobfunctiondescription: jobfunctiondescription,
					KEYN1: 0,
					KEYN2: 0,
					KEYN3: 0,
					formid: 0,
					formresp: 0
				});
				$sessionStorage.$save();
				$scope.loadNotifications();
			}, function (errorResponse) {
				$cookies.remove('AUTH');
			});
		} else{
			if($location.path() != '/eforms' &&
					$location.path() != '/passwordReset'){
				$rootScope.isLogin = true;
			}
		}
	}
	
	$scope.loadNotifications = function(){
		$http({ 
			url:"./rest/NotificationService/retrieveNotifications",
			method: "POST",
			data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid},
			headers : {'Content-Type' : 'application/json'}}) 
		.then(function( response ) {
			$rootScope.notifications = response.data;
			var count = 0;
			for(var i = 0; i < $rootScope.notifications.length; i++){
				if($rootScope.notifications[i].MsgRead != "Y"){
					count++;
				}
			}
			$rootScope.notification.unread = count;
			$state.go('dashboard');
		});
	}
	
	$scope.toDashboard = function(){
		$state.go('dashboard');
	};
	
//	$scope.init = function(){
//		alert("Calling");
//		$location.path("/login");
//	};
	
	$scope.showSettingsLink = function(){
		  var settingsDiv = document.getElementById("settingsLink");
		  $(settingsDiv).css({"visibility":"visible"});
	};
	
	$scope.hideSettingsLink = function(){
		  var settingsDiv = document.getElementById("settingsLink");
		  $(settingsDiv).css({"visibility":"hidden"});
	  };
	  
	  $scope.changeIconGrey = function(){
		  $("#notifyButton").attr("src","./images/notifications-bell-buttonB.png");
	};
	
	$scope.changeIconRed = function(){
		$("#notifyButton").attr("src","./images/notifications-bell-button.png");
	  };
	  
	 $scope.showNotifications = function(){
		  $("#notifyDiv").css({"visibility":"visible"});
		  if($rootScope.notification.unread > 0){
			  $http({ 
				url:"./rest/NotificationService/setNotifications",
				method: "POST",
				data: {"lib":$sessionStorage.datasource, "userid":$sessionStorage.userid},
				headers : {'Content-Type' : 'application/json'}}) 
				.then(function( response ) {
					for(var i = 0; i < $rootScope.notifications.length; i++){
						$rootScope.notifications[i].MsgRead == "Y";
					}
				});
			  $rootScope.notification.unread = 0;
		  }
	};
	
	$scope.hideNotifications = function(){
		$("#notifyDiv").css({"visibility":"hidden"});
	  };
	  
	  $scope.showProcess = function(){
		  $sessionStorage.KEYN1 = 0;
		  $sessionStorage.KEYN2 = 0;
		  $sessionStorage.KEYN3 = 0;
		  $sessionStorage.$save();
		  
		  $location.path('/MWS_PM/ProcessMain-en_US.html'); 
		  var url = $location.path();
		  $window.open(url); 
	  };
	  
	  $scope.clearNotes = function(){
		  $sessionStorage.KEYN1 = 0;
		  $sessionStorage.KEYN2 = 0;
		  $sessionStorage.KEYN3 = 0;
		  $sessionStorage.$save();
	  }
	  
	  $scope.$on('$locationChangeStart', function(event, next, current) {
		  var checkHash = current.indexOf("#");
		  var checkCurrent = current.substring(checkHash+1,current.length);
		  if(checkCurrent == '/formslayout'){
			  $sessionStorage.formid = 0;
			  $sessionStorage.$save();
		  }
		  if ($location.path() == '/MWS_PM/ProcessMain-en_US.html') {
			  event.preventDefault();
		  }
		  if($location.path() != '/notes'){
			  document.title = "Dashboard";
		  }
		  $scope.userid = $sessionStorage.jobfunctiondescription;
		  $rootScope.isFormSubmit = false;
		  $rootScope.isFormsModal = false;
	  });
	  
	  $rootScope.$watch('userData', function(){
		 if($rootScope.userData == undefined && $location.path() != '/eforms' && $location.path() != '/passwordReset'){
			 $http({ 
		    		url:"./rest/AdminService/getSecurity",
		    		method: "POST",
		    		data: {"datasource":$sessionStorage.datasource, "userid":$sessionStorage.userid},
		    		headers : {'Content-Type' : 'application/json'}}) 
		    	.then(function( response ) {
		    		if(response.data != ""){
		    			$rootScope.isLogin = false;
		    			$rootScope.userData = response.data;
//		    			console.log($rootScope.userData);
		    		} else {
		    			if(!$scope.isRemembered){	
		    				$rootScope.isLogin = true;
		    			}
		    			$state.go('login');
		    		}
		    		}, function( response) {
		    			$state.go('login');
		    	});
		 }
	  });
	  
	  $rootScope.$on('$stateChangeStart', function (event, nextState, currentState) {
		  if(nextState.name != 'dashboard.process'){
			  $rootScope.isFormSubmit = false;
		  }
//		  $rootScope.isFormsModal = false;
	  });
	  
	  $scope.logout = function(){
		  $scope.isRemembered = $cookies.get('AUTH');
		  if($scope.isRemembered != undefined){
			  $http({ 
	    		url:"./rest/AuthorizationService/deleteLogin",
	    		method: "POST",
	    		data: {"sessionTrack":$scope.isRemembered, "sessionToken":$sessionStorage.sessionToken, "datasource":$sessionStorage.datasource},
	    		headers : {'Content-Type' : 'application/json'}}) 
	    	.then(function( response ) {
	    		$rootScope.isLogin = true;
	    		$cookies.remove('AUTH');
	    		$sessionStorage.$reset();
	    		$state.go('login');
	    	});
		  } else{
			  $rootScope.isLogin = true;
			  $sessionStorage.$reset();
			  $state.go('login');
		  }
	  }
});

angular.module('cobraApp').factory('Messenger', function ($websocket, $sessionStorage, $rootScope) {
	// Open a WebSocket connection
//    var ws = $websocket("ws://" + document.location.host + "/ROOT/messages");
	var ws = $websocket("ws://" + document.location.host + "/messages");
    ws.onMessage(function (event) {
    	var messageRecord = angular.fromJson(event.data);
    	console.log(messageRecord);
    	$rootScope.notifications.unshift(messageRecord);
    	$rootScope.notification.unread ++;
    	if (!("Notification" in window)) {
			alert("This browser does not support desktop notification");
		} else if (Notification.permission === "granted") {
			// If it's okay let's create a notification
			if(messageRecord.MsgTitle != ""){
				var title = messageRecord.MsgTitle;
				var options = {
				      body: messageRecord.MsgText,
				      icon: "./images/justTheO.png"
				  }
				var notification = new Notification(title, options);
			}
		} else if (Notification.permission !== 'denied') {
			Notification.requestPermission(function (permission) {
				// If the user accepts, let's create a notification
				if (permission === "granted") {
					var title = messageRecord.MsgTitle;
					var options = {
					      body: messageRecord.MsgText,
					      icon: "./images/justTheO.png"
					  }
					var notification = new Notification(title, options);
				}
			});
		}
    });
    ws.onError(function (event) {
    	//console.log('connection Error', event);
    });
    ws.onClose(function (event) {
    	//console.log('connection closed', event);
    });
    ws.onOpen(function () {
    	if($sessionStorage.userid != undefined && $sessionStorage.datasource != undefined){
    		ws.send("UserID,"+$sessionStorage.userid);
    		ws.send("Lib,"+$sessionStorage.datasource);
    	}
    });
    return {
        status: function () {
        	return ws.readyState;
        },
        get: function() {
          ws.send(JSON.stringify({ action: 'get' }));
        },
        updateInfo: function(datasource, userid) {
        	ws.send("UserID,"+userid);
        	ws.send("Lib,"+datasource);
        }
    };
});

angular
